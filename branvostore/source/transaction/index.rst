.. Branvo Store API documentation master file, created by
   sphinx-quickstart on Mon Nov  5 13:20:02 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Menu:

##########
Transações
##########

**********
Consulta
**********

:guilabel:`POST` ``https://store.branvo.com/helpers/ajax/Payment/v1/consult``

.. table::
   :widths: auto

   +--------------------------+-----------------+-------------------------------------------------------------------+
   | Parameters               | type            | Descrição                                                         |
   +==========================+=================+===================================================================+
   | token*                   | string          | Token de autenticação exclusivo com 32 caracteres.                |
   +--------------------------+-----------------+-------------------------------------------------------------------+
   | codeAccount              | string          | Código de identificação do cliente com até 20 caracteres.         |
   +--------------------------+-----------------+-------------------------------------------------------------------+
   | dateStart                | string          | | Data de início do período de consulta.                          |
   |                          |                 | | Pode ser tanto no formato YYYY-mm-dd quanto dd/mm/YYYY.         |
   +--------------------------+-----------------+-------------------------------------------------------------------+
   | dateEnd                  | string          | | Data final do período de consulta.                              |
   |                          |                 | | Pode ser tanto no formato YYYY-mm-dd quanto dd/mm/YYYY.         |
   +--------------------------+-----------------+-------------------------------------------------------------------+
   | paymentMethod            | int             | | Forma de pagamento da transação.                                |
   |                          |                 | | Enviar um dos valores:                                          |
   |                          |                 | | (0: débito, 1: cartão, 2: boleto, 3: créd. BranvoPay, 4: cupom) |
   +--------------------------+-----------------+-------------------------------------------------------------------+
   | status                   | int             | | Status (situação) da transação.                                 |
   |                          |                 | | Enviar um dos valores:                                          |
   |                          |                 | | (0: pendente, 1: confirmada)                                    |
   +--------------------------+-----------------+-------------------------------------------------------------------+

.. note::
   Parâmetros com asterisco ( * ) são obrigatórios

*********************
Exemplo de Requisição
*********************

.. table::
   :widths: auto

   +--------------------------+---------------------------------------------------------------+
   | Parâmetro                | Valor                                                         |
   +==========================+===============================================================+
   | token                    | AKSJD5S84SB1B165U4O7A512X1Z54SAS6DA5SS8Q                      |
   +--------------------------+---------------------------------------------------------------+
   | codeAccount              | 1851001                                                       |
   +--------------------------+---------------------------------------------------------------+
   | dateStart                | 01/04/2018                                                    |
   +--------------------------+---------------------------------------------------------------+
   | dateEnd                  | 31/12/2018                                                    |
   +--------------------------+---------------------------------------------------------------+
   | paymentMethod            | 2                                                             |
   +--------------------------+---------------------------------------------------------------+
   | status                   | 0                                                             |
   +--------------------------+---------------------------------------------------------------+

********
Resposta
********

==============
Status 200: OK
==============

.. code:: javascript

    {
        "success": true, //retorno da requisição true/false
        "data": {
            "payments": [ //lista de transações encontradas
                {
                    "typepayment": "Boleto Bancário", //forma de pagamento
                    "status": "Pendente", //status da transação
                    "observation": "<a>2º via do boleto</a>", //observação da transação, se houver
                    "value": "R$ 250,00", //valor da transação
                    "date": "20/12/2018 10:35:15", //data e hora da transação
                    "billingdata": null, //dados de cobrança da transação, se houver
                    "codeaccount": "1851001", //código do cliente
                    "fullname": "BRANVO PAGAMENTOS" //nome completo do cliente
                }
            ]
        }
    }

.. important::
   Caso o parâmetro 'billingdata' esteja preenchido, será necessário descriptografá-lo e deserializá-lo para utilizar.

=======================
Status 400: Bad Request
=======================

Ocorre quando há algum problema nos parâmetros enviados.

.. code:: javascript

    {
        "success": false,
        "errorMessage": "Mensagem de erro correspondente"
    }