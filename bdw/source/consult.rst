.. Branvo Domain Whois documentation master file, created by
   sphinx-quickstart on Mon Nov  5 13:20:02 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Menu:

########
Consulta
########

**********
Requisição
**********

:guilabel:`POST` ``https://bdw.branvo.net/api/v1/consult/``

.. table::
   :widths: auto

   +--------------------------+-----------------+-------------------------------------------------------------------+
   | Parameters               | type            | Descrição                                                         |
   +==========================+=================+===================================================================+
   | token*                   | string          | Token de autenticação exclusivo com 32 caracteres                 |
   +--------------------------+-----------------+-------------------------------------------------------------------+
   | code                     | string          | Código de identificação do cliente com até 20 caracteres          |
   +--------------------------+-----------------+-------------------------------------------------------------------+
   | domain*                  | string          | | Nome do domínio a ser consultado, sem qualquer extensão  ou     |
   |                          |                 | | caracteres especiais, com exceção do hífen                      |
   +--------------------------+-----------------+-------------------------------------------------------------------+
   | extension*               | array           | Lista de extensões a serem consultadas                            |
   +--------------------------+-----------------+-------------------------------------------------------------------+

.. note::
   Parâmetros com asterisco ( * ) são obrigatórios

*******
Exemplo
*******

.. code:: javascript

    {
        "token":"ABCDEFGHIJKLMNOPQRSTUVWXYZ123456",
        "code":"0123456789",
        "domain":"domain",
        "extension": [
            ".com.br",
            ".com",
            ".net",
            ".info",
            ".net.br",
            ".me"
        ]
    }

********
Resposta
********

==============
Status 200: OK
==============

.. code:: javascript

    {
        "status":"success",
        "date":"2018/11/01 11:15:25",
        "data":[
            {
                "nameDomain":"domain.com",
                "available":0
            },
            {
                "nameDomain":"domain.com.br",
                "available":0
            },
            {
                "nameDomain":"domain.net",
                "available":0
            },
            {
                "nameDomain":"domain.info",
                "available":1
            }
        ]
    }

=======================
Status 400: Bad Request
=======================

Geralmente ocorre quando nenhum domínio ou extensão foi informado.

.. code:: javascript

    {
        "status": "fail",
        "date": "2018/11/01 11:25:45",
        "message": "Error message"
    }

========================
Status 401: Unauthorized
========================

Geralmente ocorre quando o token informado é inválido.

.. code:: javascript

    {
        "status": "fail",
        "date": "2018/11/01 11:15:59",
        "message": "Error message"
    }