.. Branvo Domain Whois documentation master file, created by
   sphinx-quickstart on Mon Nov  5 13:20:02 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Menu:

   consult

##########
Introdução
##########

*********
Motivação
*********

Necessidade de possuir uma aplicação que realize consultas de disponibilidade de domínios para ser utilizada na branvo store.

***************
Características
***************

- A api realiza a consulta dos principais domínios disponíveis na internet

- As consultas são armazenadas em uma base de dados.

- Todas as respostas seguem o formato JSON e com os seus respectivos códigos de status do protocolo HTTP.

=======================  =======================
Domínios não suportados
================================================
.group                   .life
.top                     .xyz
.io                      .store
=======================  =======================
