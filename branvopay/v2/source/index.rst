.. Branvo Pay API documentation master file, created by
   sphinx-quickstart on Mon Nov  5 13:20:02 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Boleto:
   :glob:

   bankslip/integration
   bankslip/notification
   bankslip/status-table

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Cartão:
   :glob:

   card/integration
   card/notification
   card/status-table

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Consultas:
   :glob:

   consult/integration

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Testes:
   :glob:

   tests/index

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Códigos de Retorno de Erros:
   :glob:

   returns/index

##########
Introdução
##########

******
Resumo
******

Esta documentação tem o objetivo de auxiliar o desenvolvedor na integração com a API da plataforma BranvoPay,
para realização de pagamentos online. Estarão descritos a seguir todos
os mecanismos necessários para a implementação dessa integração.

A API da BranvoPay foi escrita seguindo os padrões da tecnologia REST (Representational State Transfer), que
é o padrão atual do mercado e da maioria das APIs da web. Dessa maneira,
possibilitamos aos desenvolvedores uma integração que independe da linguagem de programação escolhida.

************
Metodologias
************

A integração pode ser realizada em dois endpoints: um para ambiente sandbox e um para ambiente de produção. Recomendamos iniciar a integração
em ambiente sandbox até a finalização de todos os testes necessários. Os endpoits da API BranvoPay são os seguintes:

**Sandbox**

``https://sandbox-api.branvopay.com/``

**Produção**

``https://api.branvopay.com/``

.. important::
   Para envio de requisições para o ambiente sandbox da API <https://sandbox-api.branvopay.com>, deve ser utilizado o token de testes. O token de produção será inválido neste ambiente, sendo válido apenas no ambiente de produção da API. Para obtenção do token de testes, visite a seção testes_.
   
   .. _testes: https://docs.branvo.com/branvopay/v2/tests/index.html

As requisições da API serão realizadas através de métodos HTTP. Cada um dos métodos possuirá uma responsabilidade
distinta e seguirá um padrão de requisição. Os métodos utilizados
na API BranvoPay são os seguintes:

=================== ======================= ======================================================
Método              Utilização                                Exemplo
=================== ======================= ======================================================
:guilabel:`POST`    Criação de recursos     Criar uma transação de cartão, um boleto bancário
:guilabel:`GET`     Consulta de recursos    Consultar dados de uma transação, status de um boleto
:guilabel:`PUT`     Atualização de recursos Capturar uma transação, solicitar a baixa de um boleto
=================== ======================= ======================================================

Os retornos da API sempre serão acompanhados dos códigos HTTP correspondentes ao resultado da requisição. O formato do corpo do retorno
é JSON. Os códigos HTTP utilizados pela API BranvoPay são os seguintes:

========================== ============================================================
Código                     Descrição
========================== ============================================================
*200 - OK*                  Quando uma requisição obtém o retorno com sucesso
*201 - CREATED*             .. line-block::
                             Quando uma requisição cria um recurso com sucesso
                             Ex.: Criação de boleto bancário com sucesso
*400 - BAD REQUEST*         .. line-block::
                             Quando uma requisição possui algum problema em seus dados
                             Ex.: Número de cartão inválido, CPF do cliente inválido
*401 - UNAUTHORIZED*        Problemas de autenticação
*403 - FORBIDDEN*           Problemas de autorização
*404 - NOT FOUND*           URL da requisição não for encontrada
*405 - METHOD NOT ALLOWED*  .. line-block::
                             A URL não pode ser acessada através do método solicitado
                             Ex.: Recurso acessível via GET tenta ser acessado via PUT
========================== ============================================================

************
Autenticação
************

A autenticação na API da BranvoPay é realizada através do token da conta. Em todas as requisições,
o token deve ser enviado através do campo 'Account-Token', no header de cada operação.

.. code:: javascript

    Account-Token: seu_token_aqui

******
Testes
******

Para a realização de testes na API BranvoPay, recomendamos a utilização da ferramenta POSTMAN, disponível gratuitamente no link:
<https://www.getpostman.com/>. Para mais detalhes sobre a operação de testes na API, visite a seção de testes_.

.. _testes: https://docs.branvo.com/branvopay/v2/tests/index.html

.. note::
   A API Branvo Pay trabalha com um sistema de cache para requisições em curto intervalo de tempo. Recomendamos intervalo de 1 segundo entre requisições do mesmo tipo.

*********
Glossário
*********

O presente glossário visa explicar alguns termos utilizados nesta documentação,
para facilitar o entendimento por parte de todos que realizarem a leitura e evitar
dúvidas no momento da integração.

*Cliente/Comprador* – É o cliente final da loja, aquele que comprou e realizou o
pagamento com seu cartão de crédito/boleto.

*Loja/Usuário BranvoPay* – É o usuário cadastrado na plataforma BranvoPay, aquele
que recebe os pagamentos. (Estabelecimentos)

*Entrada confirmada* – O boleto foi enviado para o banco e o seu registro foi confirmado, mas esse
status **NÃO** significa que o boleto foi pago.

*Liquidação/Liquidado* – Ato de pagamento do boleto/Boleto liquidado. Este é o status que **GARANTE**
que boleto foi pago.

*Baixa/Baixado* – Ato de cancelamento do boleto/Boleto cancelado. Este status garante que o boleto
foi **CANCELADO**.

*Transação autorizada/aprovada* – A transação foi enviada à
operadora/adquirente e foi apenas **AUTORIZADA**, ou seja, recebemos o OK para
continuar com a operação, mas ela ainda não foi debitada/creditada e enviada
para a fatura do cliente. O real crédito/débito da transação acontece no momento
da **CAPTURA**, podendo ela ser automática ou não, conforme descrito
posteriormente. Este status NÃO garante a confirmação do pagamento.

*Transação capturada* – A transação foi enviada à operadora/adquirente e foi
**CAPTURADA**, ou seja, neste momento é que o valor foi creditado/debitado do
cliente. Atente para iniciar a logística posterior ao pagamento apenas quando a
transação estiver com este status. Este status GARANTE a confirmação do
pagamento, sendo que status anteriores a este NÃO garantem.

*Split* – Ação de realizar uma divisão de valores. Nesta modalidade, o boleto é emitido por uma
conta principal, porém ao ser liquidado/baixado, o valor é dividido entre as contas desejadas.