***************************
Operação dos Testes na API
***************************

Para transacionar na API da Branvo Pay em ambiente sandbox, você precisa utilizar o seu token de testes. Para obtê-lo, você deve acessar o painel da Branvo Pay <https://app.branvopay.com> com seu usuário e senha e seguir os seguintes passos:

.. note::
    Caso você ainda não possua um usuário para acesso ao painel, envie um e-mail para <suporte@branvo.com> solicitando o seu acesso.

1. Acesse a tela de configurações:

.. image:: conf.png

2. Na tela de configurações, clique no link "Mostrar" no campo "Token de testes":

.. image:: mostrar.png

3. Para visualizar as transações realizadas com o token de testes, você precisa ativar a opção "Ativar Sandbox", logo acima do campo do token de testes. Essa opção ativa o ambiente sandbox da plataforma, onde ela passa a operar apenas com o seu token de testes em ambiente sandbox.

.. image:: sandbox.png

.. important::
   Para envio de requisições para o ambiente sandbox da API <https://sandbox-api.branvopay.com>, deve ser utilizado o token de testes. O token de produção será inválido neste ambiente, sendo válido apenas no ambiente de produção da API.
