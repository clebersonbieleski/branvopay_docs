.. Branvo Domain Whois documentation master file, created by
   sphinx-quickstart on Mon Nov  5 13:20:02 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Boleto:

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Cartão:
   
################
Tabela de status
################

.. table::
   :widths: auto

   +------------------+---------------------------------+------------------------------------------------------+
   | Código           | Status                          | Descrição                                            |
   +==================+=================================+======================================================+
   | 0                | **PENDENTE**                    | | A transação está na fila de processamento          |
   |                  |                                 | | e será processada em breve                         |
   +------------------+---------------------------------+------------------------------------------------------+
   | 1                | **AGUARDANDO**                  | | A transação está aguardando análise e aprovação    |
   |                  |                                 | | ou reprovação por parte do cliente                 |
   +------------------+---------------------------------+------------------------------------------------------+
   | 10               | **TRANSAÇÃO NÃO AUTORIZADA**    | | A transação não foi autorizada pela loja/usuário   |
   |                  |                                 | | BranvoPay, após análise                            |
   +------------------+---------------------------------+------------------------------------------------------+
   | 11               | **TRANSAÇÃO NÃO AUTORIZADA**    | | A transação não foi autorizada                     |
   +------------------+---------------------------------+------------------------------------------------------+
   | 12               | **TRANSAÇÃO NÃO AUTORIZADA**    | | A transação não foi autorizada pela operadora      |
   |                  |                                 | | de cartão                                          |
   +------------------+---------------------------------+------------------------------------------------------+
   | 13               | **TRANSAÇÃO NÃO AUTORIZADA**    | | A transação não foi autorizada pela Branvo         |
   +------------------+---------------------------------+------------------------------------------------------+
   | 20               | **TRANSAÇÃO APROVADA**          | | A transação foi aprovada pela loja/usuário         |
   |                  |                                 | | BranvoPay, após análise                            |
   +------------------+---------------------------------+------------------------------------------------------+
   | 21               | **TRANSAÇÃO APROVADA**          | | A transação foi aprovada                           |
   +------------------+---------------------------------+------------------------------------------------------+
   | 22               | **TRANSAÇÃO APROVADA**          | | A transação foi aprovada pela operadora de cartão  |
   +------------------+---------------------------------+------------------------------------------------------+
   | 23               | **TRANSAÇÃO APROVADA**          | | A transação foi aprovada pela Branvo               |
   +------------------+---------------------------------+------------------------------------------------------+
   | 30               | **TRANSAÇÃO CANCELADA**         | | A transação foi cancelada pela loja/usuário        |
   |                  |                                 | | BranvoPay                                          |
   +------------------+---------------------------------+------------------------------------------------------+
   | 31               | **SOLICITAÇÃO DE CANCELAMENTO** | | O comprador solicitou o cancelamento               |
   +------------------+---------------------------------+------------------------------------------------------+
   | 32               | **TRANSAÇÃO CANCELADA**         | | A transação foi cancelada pela operadora de cartão |
   +------------------+---------------------------------+------------------------------------------------------+
   | 33               | **TRANSAÇÃO CANCELADA**         | | A transação foi cancelada pela Branvo              |
   +------------------+---------------------------------+------------------------------------------------------+
   | 40               | **TRANSAÇÃO CAPTURADA**         | | A transação foi capturada pela loja/usuário        |
   |                  |                                 | | BranvoPay                                          |
   +------------------+---------------------------------+------------------------------------------------------+
   | 41               | **TRANSAÇÃO CAPTURADA**         | | A transação foi capturada sem passar por           |
   |                  |                                 | | autorização                                        |
   +------------------+---------------------------------+------------------------------------------------------+
   | 43               | **TRANSAÇÃO CAPTURADA**         | | A transação foi capturada pela Branvo              |
   +------------------+---------------------------------+------------------------------------------------------+