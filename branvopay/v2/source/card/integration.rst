.. Branvo Pay documentation master file, created by
   sphinx-quickstart on Mon Nov  5 13:20:02 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Boleto:

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Cartão:

################################
Cartão de Crédito - Integrações
################################

**************
Nova Transação
**************

:guilabel:`POST` ``https://api.branvopay.com/cartao/v2/new``

**Endpoint utilizado para realizar uma transação de cartão de crédito**

==========
Requisição
==========

.. table::
   :widths: 70 20 150

   +----------------------------+-----------------+------------------------------------------------------------+
   | **Header**                 | **Tipo**        | **Descrição**                                              |
   +----------------------------+-----------------+------------------------------------------------------------+
   | Account-Token*             | string          | | Token da conta na BranvoPay                              |
   +----------------------------+-----------------+------------------------------------------------------------+
   | **Body**                   | **Tipo**        | **Descrição**                                              |
   +----------------------------+-----------------+------------------------------------------------------------+
   | clientName*                | string          | | Nome completo do comprador sem caracteres especiais      |
   |                            |                 | | contendo no máximo 40 caracteres                         |
   +----------------------------+-----------------+------------------------------------------------------------+
   | clientType*                | string          | Tipo de pessoa do comprador. [PF, PJ]                      |
   +----------------------------+-----------------+------------------------------------------------------------+
   | clientDocument*            | string          | Documento do comprador [CPF, CNPJ]                         |
   +----------------------------+-----------------+------------------------------------------------------------+
   | clientMail*                | string          | Email do comprador                                         |
   +----------------------------+-----------------+------------------------------------------------------------+
   | clientPhone*               | string          | Telefone do comprador. Necessário informar o DDD           |
   +----------------------------+-----------------+------------------------------------------------------------+
   | clientSecondaryMail        | string          | Email secundário do comprador                              |
   +----------------------------+-----------------+------------------------------------------------------------+
   | clientBirthDate            | string          | | Data de nascimento do comprador                          |
   |                            |                 | | Formato: YYYY-mm-dd                                      |
   +----------------------------+-----------------+------------------------------------------------------------+
   | billingStreet*             | string          | | Rua do endereço de cobrança do comprador                 |
   |                            |                 | | contendo no máximo 30 caracteres                         |
   +----------------------------+-----------------+------------------------------------------------------------+
   | billingNumber*             | string          | | Número do endereço de cobrança do comprador              |
   |                            |                 | | contendo no máximo 10 caracteres                         |
   +----------------------------+-----------------+------------------------------------------------------------+
   | billingNeighbourhood*      | string          | | Bairro do endereço de cobrança do comprador              |
   |                            |                 | | contendo no máximo 20 caracteres                         |
   +----------------------------+-----------------+------------------------------------------------------------+
   | billingCity*               | string          | | Cidade do endereço de cobrança do comprador              |
   |                            |                 | | contendo no máximo 20 caracteres                         |
   +----------------------------+-----------------+------------------------------------------------------------+
   | billingState*              | string          | Sigla do estado do endereço de cobrança do comprador       |
   +----------------------------+-----------------+------------------------------------------------------------+
   | billingPostcode*           | string          | Cep do endereço de cobrança do comprador                   |
   +----------------------------+-----------------+------------------------------------------------------------+
   | billingComplement          | string          | | Complemento do endereço de cobrança do comprador         |
   |                            |                 | | contendo no máximo 30 caracteres                         |
   +----------------------------+-----------------+------------------------------------------------------------+
   | billingResponsible*        | string          | | Responsável do endereço de cobrança do comprador         |
   |                            |                 | | contendo no máximo 40 caracteres                         |
   +----------------------------+-----------------+------------------------------------------------------------+
   | deliveryStreet*            | string          | | Rua do endereço de entrega do comprador                  |
   |                            |                 | | contendo no máximo 30 caracteres                         |
   +----------------------------+-----------------+------------------------------------------------------------+
   | deliveryNumber*            | string          | | Número do endereço de entrega do comprador               |
   |                            |                 | | contendo no máximo 10 caracteres                         |
   +----------------------------+-----------------+------------------------------------------------------------+
   | deliveryNeighbourhood*     | string          | | Bairro do endereço de entrega do comprador               |
   |                            |                 | | contendo no máximo 20 caracteres                         |
   +----------------------------+-----------------+------------------------------------------------------------+
   | deliveryCity*              | string          | | Cidade do endereço de entrega do comprador               |
   |                            |                 | | contendo no máximo 20 caracteres                         |
   +----------------------------+-----------------+------------------------------------------------------------+
   | deliveryState*             | string          | Sigla do estado do endereço de entrega do comprador        |
   +----------------------------+-----------------+------------------------------------------------------------+
   | deliveryPostcode*          | string          | Cep do endereço de entrega do comprador                    |
   +----------------------------+-----------------+------------------------------------------------------------+
   | deliveryComplement         | string          | | Complemento do endereço de entrega do comprador          |
   |                            |                 | | contendo no máximo 30 caracteres                         |
   +----------------------------+-----------------+------------------------------------------------------------+
   | deliveryResponsible*       | string          | | Responsável do endereço de entrega do comprador          |
   |                            |                 | | contendo no máximo 40 caracteres                         |
   +----------------------------+-----------------+------------------------------------------------------------+
   | cardOwnerName*             | string          | | Nome completo do proprietário do cartão sem caracteres   |
   |                            |                 | | especiais, contendo no máximo 40 caracteres              |
   +----------------------------+-----------------+------------------------------------------------------------+
   | cardOwnerType*             | string          | Tipo de pessoa do proprietário do cartão. [PF, PJ]         |
   +----------------------------+-----------------+------------------------------------------------------------+
   | cardOwnerDocument*         | string          | Documento do proprietário do cartão [CPF, CNPJ]            |
   +----------------------------+-----------------+------------------------------------------------------------+
   | cardOwnerPhone*            | string          | | Telefone do proprietário do cartão.                      |
   |                            |                 | | Necessário informar o DDD                                |
   +----------------------------+-----------------+------------------------------------------------------------+
   | cardFlag*                  | string          | Bandeira do cartão                                         |
   +----------------------------+-----------------+------------------------------------------------------------+
   | cardNumber*                | string          | Número do cartão                                           |
   +----------------------------+-----------------+------------------------------------------------------------+
   | cardMonth*                 | string          | Mês de vencimento do cartão                                |
   +----------------------------+-----------------+------------------------------------------------------------+
   | cardYear*                  | string          | Ano de vencimento do cartão                                |
   +----------------------------+-----------------+------------------------------------------------------------+
   | cardCode*                  | string          | Código de segurança do cartão                              |
   +----------------------------+-----------------+------------------------------------------------------------+
   | antiFraud*                 | int             | | 0 (false) ou 1 (true)                                    |
   |                            |                 | | Passar por análise automática de antifraude?             |
   |                            |                 | | OBS.: Caso a transação passe da análise com 0 ponto,     |
   |                            |                 | | ela irá automaticamente para a fila de processamento     |
   +----------------------------+-----------------+------------------------------------------------------------+
   | orderNumber*               | string          | Número do pedido                                           |
   +----------------------------+-----------------+------------------------------------------------------------+
   | value*                     | string          | Valor do pedido                                            |
   +----------------------------+-----------------+------------------------------------------------------------+
   | parcelNumber*              | int             | | Número de parcelas. [1-12]                               |
   |                            |                 | | Utilizar 1 para à vista                                  |
   +----------------------------+-----------------+------------------------------------------------------------+
   | capture*                   | int             | | 0 (false) ou 1 (true)                                    |
   |                            |                 | | Realizar a captura automática?                           |
   |                            |                 | | Se for true, a transação será descontada do comprador no |
   |                            |                 | | momento da transação, caso for aprovada                  |
   |                            |                 | | OBS.: A captura automática só poderá ser ativada caso    |
   |                            |                 | | não houver análise antifraude                            |
   +----------------------------+-----------------+------------------------------------------------------------+
   | abbreviate                 | int             | | 0 (false) ou 1 (true)                                    |
   |                            |                 | | Ativar a abreviação automática dos dados enviados?       |
   |                            |                 | | Caso essa opção seja acionada, a api não retornará erros |
   |                            |                 | | referentes à limitação de tamanho dos campos, abreviando |
   |                            |                 | | automaticamente os dados quando o limite de caracteres   |
   |                            |                 | | de algum campo for ultrapassado                          |
   |                            |                 | | Ex: Avenida Governador Pedro Gomes Terceiro              |
   |                            |                 | | É abreviado para Av Gov Pedro Gomes III                  |
   +----------------------------+-----------------+------------------------------------------------------------+
   | ipAddress                  | string          | Endereço ip do comprador                                   |
   +----------------------------+-----------------+------------------------------------------------------------+
   | os                         | string          | Sistema operacional do computador do comprador             |
   +----------------------------+-----------------+------------------------------------------------------------+
   | notificationUrl            | string          | | URL para notificação de atualização da transação         |
   |                            |                 | | Ver sessão “URL de notificação”                          |
   +----------------------------+-----------------+------------------------------------------------------------+
   | browser                    | string          | Navegador utilizado pelo comprador                         |
   +----------------------------+-----------------+------------------------------------------------------------+

.. note::
   Parâmetros com asterisco ( * ) são obrigatórios

========
Resposta
========

**Sucesso**

.. code:: javascript

   {
      "success": true,
      "data": {
         "code": int,
         "accountName": string,
         "payerName": string,
         "orderNumber": string,
         "status": int,
         "amount": decimal,
         "fraudPoints": int,
         "dateAnalysis": string,
         "message": string
      }
   }

**Falha**

.. code:: javascript

   {
      "success": false,
      "errorCode": string,
      "errorMessage": string
   }

=======
Exemplo
=======

**Requisição**

.. table::
   :widths: 70 320

   =========================== =================================
   **Header**                  **Valor enviado**
   Account-Token*              \********************************
   **Body**                    **Valor enviado**
   clientName*                 nome completo
   clientType*                 PF
   clientDocument*             000\.000\.000-00
   clientMail*                 contato@exemplo.com
   clientPhone*                \(00\) 0000-0000
   clientSecondaryMail         contato2@exemplo.com
   clientBirthDate             0000-00-00
   billingStreet*              rua
   billingNumber*              00
   billingNeighbourhood*       bairro
   billingCity*                cidade
   billingState*               sigla do estado
   billingPostcode*            00000-000
   billingComplement           complemento
   billingResponsible*         nome completo
   deliveryStreet*             rua
   deliveryNumber*             00
   deliveryNeighbourhood*      bairro
   deliveryCity*               cidade
   deliveryState*              sigla do estado
   deliveryPostcode*           00000-000
   deliveryComplement          complemento
   deliveryResponsible*        nome completo
   cardOwnerName*              nome completo
   cardOwnerType*              PJ
   cardOwnerDocument*          00\.000\.000/0000-00
   cardOwnerPhone*             \(00\) 00000-0000
   cardFlag*                   bandeira
   cardNumber*                 0000\.0000\.0000\.0000
   cardMonth*                  00
   cardYear*                   0000
   cardCode*                   000
   antiFraud*                  1
   orderNumber*                11111
   value*                      50.00
   parcelNumber*               10
   capture*                    1
   ipAddress                   192.168.0.0
   os                          sistema operacional
   notificationUrl             url.exemplo.com
   browser                     navegador
   =========================== =================================

**Resposta**

.. code:: javascript

   {
       "success": true,
       "data": {
           "code": 123,
           "accountName": "CONTA DA TRANSAÇÃO",
           "payerName": "NOME COMPLETO",
           "orderNumber": "11111",
           "status": 41,
           "amount": "50.00",
           "fraudPoints": 0,
           "dateAnalysis": "11/12/2018 10:14:06",
           "message": "Transação realizada com sucesso"
       }
   }

******************
Split de Pagamento
******************

:guilabel:`POST` ``https://api.branvopay.com/cartao/v2/split``

**Endpoint utilizado para realizar uma transação na modalidade de Split de Pagamentos**

O Split de Pagamentos é um sistema que realiza as divisões dos pagamentos e taxas entre duas ou mais contas Branvo Pay, realizando uma única cobrança do seu
cliente e simplificando o gerenciamento de recebíveis. Você pode informar quantas contas forem necessárias para dividir os pagamentos, especificando a quantia
que cada conta deve receber, em forma de valor líquido ou porcentagem. Você também pode diferenciar quem pagará a taxa da transação: se é só a conta principal ou
se ela será dividida entre todos os participantes do split.

==========
Requisição
==========

.. important::
   A requisição para Split de Pagamentos requer o envio de todos os parâmetros de uma nova transação comum (new), juntamente com os parâmetros da tabela abaixo:

.. table::
   :widths: 70 20 150

   +----------------------------+-----------------+------------------------------------------------------------------------+
   | **Body**                   | **Tipo**        | **Descrição**                                                          |
   +============================+=================+========================================================================+
   | taxRule                    | string          | | Regra de desconto de taxas do split                                  |
   |                            |                 | | Valores aceitos: [`divided` (todas as contas envolvidas pagarão      |
   |                            |                 | | a taxa, ou `main` (apenas a conta principal pagará a taxa)].         |
   |                            |                 | | Caso não informado, assumirá o valor `divided`                       |
   +----------------------------+-----------------+------------------------------------------------------------------------+
   | splitType*                 | string          | | Os valores enviados serão em formato valor ou                        |
   |                            |                 | | porcentagem                                                          |
   |                            |                 | | Valores aceitos: [`val` (valor), ou `perc` (porcentagem)]            |
   +----------------------------+-----------------+------------------------------------------------------------------------+
   | splitAmount*               | array           | | Array com valores a serem repartidos                                 |
   |                            |                 | | Ex.: splitAmount[0] = 100.00, splitAmount[1] = 50.00...              |
   +----------------------------+-----------------+------------------------------------------------------------------------+
   | splitToken*                | array           | | Array com tokens das contas que irão receber os valores              |
   |                            |                 | | **OBS.:** Respeitar a ordem do parâmetro splitAmount,                |
   |                            |                 | | Ex.: Se splitAmount[0] = 100.00 e splitToken[0] = 123                |
   |                            |                 | | a conta referente ao token 123 receberá R$ 100,00,                   |
   |                            |                 | | e assim por diante                                                   |
   +----------------------------+-----------------+------------------------------------------------------------------------+

.. note::
   Parâmetros com asterisco ( * ) são obrigatórios

.. note::
   O tamanho dos arrays splitToken e splitAmount deve ser igual. Para splitType = perc, ou seja, valores divididos por porcentagem, a soma dos elementos de splitAmount
   deve totalizar 100 (100%). Para splitType = val, ou seja, divisão de valores liquidos, a soma dos elementos de splitAmount deve ser igual ao valor total da transação
   enviado no parâmetro 'value'.


========
Resposta
========

**Sucesso**

.. code:: javascript

   {
      "success": true,
      "data": {
         "code": int,
         "orderNumber": string,
         "status": int,
         "amount": decimal,
         "fraudPoints": int,
         "dateAnalysis": string,
         "queueOrder": int,
         "retry": bool,
         "message": string,
         "split": [
               {
                  "name": string,
                  "amount": string
               },
               {
                  "name": string,
                  "amount": string
               },
               ...
         ]
      }
   }

**Falha**

.. code:: javascript

   {
      "success": false,
      "errorCode": string,
      "errorMessage": string
   }

=======
Exemplo
=======

**Requisição**

.. table::
   :widths: 70 320

   =========================== =================================
   **Header**                  **Valor enviado**
   Account-Token*              \********************************
   **Body**                    **Valor enviado**
   clientName*                 nome completo
   clientType*                 PF
   clientDocument*             000\.000\.000-00
   clientMail*                 contato@exemplo.com
   clientPhone*                \(00\) 0000-0000
   clientSecondaryMail         contato2@exemplo.com
   clientBirthDate             0000-00-00
   billingStreet*              rua
   billingNumber*              00
   billingNeighbourhood*       bairro
   billingCity*                cidade
   billingState*               sigla do estado
   billingPostcode*            00000-000
   billingComplement           complemento
   billingResponsible*         nome completo
   deliveryStreet*             rua
   deliveryNumber*             00
   deliveryNeighbourhood*      bairro
   deliveryCity*               cidade
   deliveryState*              sigla do estado
   deliveryPostcode*           00000-000
   deliveryComplement          complemento
   deliveryResponsible*        nome completo
   cardOwnerName*              nome completo
   cardOwnerType*              PJ
   cardOwnerDocument*          00\.000\.000/0000-00
   cardOwnerPhone*             \(00\) 00000-0000
   cardFlag*                   bandeira
   cardNumber*                 0000\.0000\.0000\.0000
   cardMonth*                  00
   cardYear*                   0000
   cardCode*                   000
   antiFraud*                  1
   orderNumber*                11111
   value*                      50.00
   parcelNumber*               10
   capture*                    1
   ipAddress                   192.168.0.0
   os                          sistema operacional
   notificationUrl             url.exemplo.com
   browser                     navegador
   splitType                   val
   splitToken[0]               012345678910
   splitToken[1]               112345678910
   splitAmount[0]              25.00
   splitAmount[1]              25.00
   =========================== =================================

**Resposta**

.. code:: javascript

   {
      "success": true,
      "data": {
         "code": 1,
         "accountName": "CONTA PRINCIPAL DA TRANSAÇÃO",
         "payerName": "NOME COMPLETO",
         "orderNumber": "11111",
         "status": 41,
         "amount": 50.00,
         "fraudPoints": 0,
         "dateAnalysis": "19/11/2018 11:42:34",
         "message":"Operation successful",
         "split": [
            {
               "name": "conta que receberá valores 1",
               "amount": "R$ 25.00"
            },
            {
               "name": "conta que receberá valores 2",
               "amount": "R$ 25.00"
            }
         ]
      }
   }



************
Cancelamento
************

:guilabel:`PUT` ``https://api.branvopay.com/cartao/v2/cancel/{orderNumber}``

**Realiza o cancelamento e estorno da operação através do parâmetro orderNumber recebido, sendo que orderNumber é o número do pedido da loja.**

.. note::
   O cancelamento e estorno de transações está disponível apenas para transações AUTORIZADAS ou CAPTURADAS.

.. attention::

    Deve ser utilizado o mesmo orderNumber enviado para criar a transação.
    Necessário enviar via header o token da conta referente à transação

==========
Requisição
==========

.. table::
   :widths: 70 20 150

   +----------------------------+-----------------+------------------------------------------------------------+
   | Header                     | Tipo            | Descrição                                                  |
   +============================+=================+============================================================+
   | Account-Token*             | string          | Token da conta referente à transação                       |
   +----------------------------+-----------------+------------------------------------------------------------+

.. note::
   Parâmetros com asterisco ( * ) são obrigatórios

========
Resposta
========

**Sucesso**

.. code:: javascript

   {
      "success":true,
      "data": [
         {
            "code":int,
            "accountName":string,
            "orderNumber":string,
            "status":int,
            "date":string,
            "message":"string"
         }
      ]
   }

**Falha**

.. code:: javascript

   {
      "success": false,
      "errorCode": string,
      "errorMessage": string
   }

=======
Exemplo
=======

**Requisição**

.. parsed-literal::
   **URL:** https://sandbox-api.branvopay.com/cartao/v2/cancel/11111

.. table::
   :widths: 70 320

   =========================== ===========================================================
   **Header**                  **Valor enviado**
   Account-Token               \********************************
   =========================== ===========================================================

**Resposta**

.. code:: javascript

   {
       "success": true,
       "data": [
           {
               "code": 123,
               "accountName": "CONTA DA TRANSAÇÃO",
               "orderNumber": "11111",
               "status": 32,
               "date": "19/11/2018 11:42:34",
               "message": "Transação cancelada com sucesso"
           }
       ]
   }





****************
Estorno Parcial
****************

:guilabel:`PUT` ``https://api.branvopay.com/cartao/v2/cancel/{orderNumber}?amount=x.xx``

**Realiza o cancelamento e estorno da operação através do parâmetro orderNumber recebido, sendo que orderNumber é o número do pedido da loja. O estorno parcial consiste em cancelar apenas uma parte do valor da transação, sendo que a outra parte permanece capturada.**

.. note::
   O estorno parcial de transações está disponível apenas para transações CAPTURADAS.

.. attention::

    Deve ser utilizado o mesmo orderNumber enviado para criar a transação.
    Necessário enviar via header o token da conta referente à transação

==========
Requisição
==========

.. table::
   :widths: 70 20 150

   +----------------------------+-----------------+------------------------------------------------------------+
   | **Query Param**            | **Tipo**        | **Descrição**                                              |
   +============================+=================+============================================================+
   | amount*                    | decimal         | Valor a ser estornado                                      |
   +----------------------------+-----------------+------------------------------------------------------------+

.. note::
   Parâmetros com asterisco ( * ) são obrigatórios

========
Resposta
========

**Sucesso**

.. code:: javascript

   {
      "success":true,
      "data": [
         {
            "code":int,
            "accountName":string,
            "orderNumber":string,
            "value":string,
            "totalVoided":string,
            "status":int,
            "date":string,
            "message":"string"
         }
      ]
   }

**Falha**

.. code:: javascript

   {
      "success": false,
      "errorCode": string,
      "errorMessage": string
   }

=======
Exemplo
=======

**Requisição**

.. parsed-literal::
   **URL:** https://sandbox-api.branvopay.com/cartao/v2/cancel/11111?amount=10.00

.. table::
   :widths: 70 320

   =========================== ===========================================================
   **Header**                  **Valor enviado**
   Account-Token               \********************************
   =========================== ===========================================================

**Resposta**

.. code:: javascript

   {
       "success": true,
       "data": [
           {
               "code": 123,
               "accountName": "CONTA DA TRANSAÇÃO",
               "orderNumber": "11111",
               "status": 41,
               "value": 100.00,
               "totalVoided": 10.00,
               "date": "19/11/2018 11:42:34",
               "message": "Estorno parcial realizado com sucesso"
           }
       ]
   }

.. note::
   O campo `status` só conterá um status de cancelado quando o valor total da transação for estornado. Caso contrário, ela permanecerá como capturada.

.. note::
   O campo `totalVoided` conterá sempre o valor total já estornado da transação.




********
Consulta
********

:guilabel:`GET` ``https://api.branvopay.com/cartao/v2/get/{orderNumber}``

**Realiza a consulta da operação através do parâmetro orderNumber, que é o número do pedido da loja.**

==========
Requisição
==========

.. table::
   :widths: 70 20 150

   +----------------------------+-----------------+------------------------------------------------------------+
   | Header                     | Tipo            | Descrição                                                  |
   +============================+=================+============================================================+
   | Account-Token*             | string          | Token da conta referente à transação                       |
   +----------------------------+-----------------+------------------------------------------------------------+

.. note::
   Parâmetros com asterisco ( * ) são obrigatórios

========
Resposta
========

**Sucesso**

.. code:: javascript

   {
       "success": true,
       "data": {
           "code": int,
           "ordernumber": string,
           "status": int,
           "statusdescription": string,
           "clientname": string,
           "clientdocument": string,
           "reference": string,
           "responsible": string,
           "fraudpoints": string,
           "parcelnumber": int,
           "value": decimal,
           "ipaddress": string,
           "os": string,
           "browser": string,
           "dateanalysis": string,
           "notificationurl": string,
           "datecreate": string,
           "creditCard": {
               "number": string,
               "holder": string,
               "date": string
           },
           "address": {
               "street": string,
               "number": string,
               "complement": string,
               "neighbourhood": string,
               "city": string,
               "state": string
           },
           "billingAddress": {
               "street": string
               "number": string
               "complement": string
               "neighbourhood": string
               "city": string
               "state": string
           },
           "accountName": string
       }
   }

**Falha**

.. code:: javascript

   {
      "success": false,
      "errorCode": string,
      "errorMessage": string
   }

=======
Exemplo
=======

**Requisição**

.. parsed-literal::
   **URL:** https://sandbox-api.branvopay.com/cartao/v2/get/11111

.. table::
   :widths: 70 320

   =========================== ===========================================================
   **Header**                  **Valor enviado**
   Account-Token               \********************************
   =========================== ===========================================================

**Resposta**

.. code:: javascript

   {
       "success": true,
       "data": {
           "code": 123,
           "ordernumber": "11111",
           "status": "32",
           "statusdescription": "Operation Successful",
           "clientname": "NOME COMPLETO",
           "clientdocument": "000.000.000-00",
           "reference": "",
           "responsible": "NOME COMPLETO",
           "fraudpoints": "0",
           "parcelnumber": "10",
           "value": "50.00",
           "ipaddress": "192.168.0.0",
           "os": "windows",
           "browser": "chrome",
           "dateanalysis": "19/11/2018 11:42:34",
           "notificationurl": "",
           "datecreate": "19/11/2018 11:42:30",
           "creditCard": {
               "number": "492970******1300",
               "holder": "NOME COMPLETO",
               "date": "8/2020"
           },
           "address": {
               "street": "RUA",
               "number": "00",
               "complement": "COMPLEMENTO",
               "neighbourhood": "BAIRRO",
               "city": "CIDADE",
               "state": "SIGLA DO ESTADO"
           },
           "billingAddress": {
               "street": "RUA",
               "number": "00",
               "complement": "COMPLEMENTO",
               "neighbourhood": "BAIRRO",
               "city": "CIDADE",
               "state": "SIGLA DO ESTADO"
           },
           "accountName": "CONTA DA TRANSAÇÃO"
       }
   }

*******
Captura
*******

:guilabel:`PUT` ``https://api.branvopay.com/cartao/v2/capture/{orderNumber}``

**Realiza a captura operação através do parâmetro orderNumber recebido, que é o número do pedido da loja.**

==========
Requisição
==========

.. table::
   :widths: 70 20 150

   +----------------------------+-----------------+------------------------------------------------------------+
   | Header                     | Tipo            | Descrição                                                  |
   +============================+=================+============================================================+
   | Account-Token*             | string          | Token da conta referente à transação                       |
   +----------------------------+-----------------+------------------------------------------------------------+

.. note::
   Parâmetros com asterisco ( * ) são obrigatórios

========
Resposta
========

**Sucesso**

.. code:: javascript

   {
      "success":true,
      "data": [
         {
            "code":int,
            "accountName":string,
            "orderNumber":string,
            "status":int,
            "date":string,
            "message":"string"
         }
      ]
   }

**Falha**

.. code:: javascript

   {
      "success": false,
      "errorCode": string,
      "errorMessage": string
   }

=======
Exemplo
=======

**Requisição**

.. parsed-literal::
   **URL:** https://sandbox-api.branvopay.com/cartao/v2/capture/11111

.. table::
   :widths: 70 320

   =========================== ===========================================================
   **Header**                  **Valor enviado**
   Account-Token               \********************************
   =========================== ===========================================================

**Resposta**

.. code:: javascript

   {
       "success": true,
       "data": {
           "code": 123,
           "accountName": "CONTA DA TRANSAÇÃO",
           "payerName": "NOME COMPLETO",
           "orderNumber": "11111",
           "status": 40,
           "amount": "50.00",
           "fraudPoints": "0",
           "dateAnalysis": "06/12/2018 15:01:18",
           "message": "Operation Successful"
       }
   }

***********
Recorrência
***********

:guilabel:`POST` ``https://api.branvopay.com/cartao/v2/recurrent``

**Endpoint utilizado para criar uma Transação Recorrente**

Uma transação recorrente é uma cobrança que será feita automaticamente pela Branvo Pay, no período informado pelo cliente. É o método utilizado por clubes de assinatura
e assinatura de serviços online que possuem pagamento mensal/anual.

==========
Requisição
==========

.. important::
   A requisição para Recorrência requer o envio de todos os parâmetros de uma nova transação comum (new), juntamente com os parâmetros da tabela abaixo:

.. table::
   :widths: 70 20 150

   +----------------------------+-----------------+------------------------------------------------------------+
   | **Body**                   | **Tipo**        | **Descrição**                                              |
   +============================+=================+============================================================+
   | frequence*                 | string          | | Frequência de cobrança                                   | 
   |                            |                 | |                                                          |
   |                            |                 | | Valores aceitos:                                         |
   |                            |                 | | [monthly (mensal), bimonthly (cada 2 meses),             |
   |                            |                 | | quarterly (trimestral), semester (semestral),            |
   |                            |                 | | yearly (anual)]                                          |
   +----------------------------+-----------------+------------------------------------------------------------+
   | finalDate                  | string          | | Data final da recorrência. Ela será executada            |
   |                            |                 | | automaticamente até esta data.                           |
   +----------------------------+-----------------+------------------------------------------------------------+
   | startDate                  | string          | | Data de início da recorrência. Ela será executada        |
   |                            |                 | | automaticamente a partir desta data e no mesmo dia dos   |
   |                            |                 | | períodos subsequentes.                                   |
   |                            |                 | | Quando a data de início não for informada, a recorrência |
   |                            |                 | | será iniciada na data atual.                             |
   +----------------------------+-----------------+------------------------------------------------------------+

.. note::
   Parâmetros com asterisco ( * ) são obrigatórios

.. note::
   Os dados de transação enviados nesta requisição servirão como base para as próximas cobranças. É possível atualizar os dados da recorrência, como explicado posteriormente
   nesta documentação. Esta transação pode ser capturada automaticamente ou não, conforme parâmetro 'capture' recebido. Fica sob responsabilidade do cliente Branvo Pay capturar
   esta transação posteriormente. As próximas transações da recorrência sempre serão capturadas automaticamente.

========
Resposta
========

**Sucesso**

.. code:: javascript

   {
      "success":true,
      "data": [
         {
            "code": int,
            "accountName": string,
            "payerName": string,
            "orderNumber": string,
            "status": int,
            "amount": decimal,
            "fraudPoints": int,
            "dateAnalysis": string,
            "message": string,
            "recurrence": {
                  "active": bool,
                  "amount": decimal,
                  "carddate": string,
                  "cardname": string,
                  "cardnumber": string,
                  "code": int,
                  "datecreate": string,
                  "datelasttransaction": string,
                  "finaldate": string,
                  "frequence": string
            }
         }
      ]
   }

**Falha**

.. code:: javascript

   {
      "success": false,
      "errorCode": string,
      "errorMessage": string
   }

=======
Exemplo
=======

**Requisição**

.. table::
   :widths: 70 320

   =========================== =================================
   **Header**                  **Valor enviado**
   Account-Token*              \********************************
   **Body**                    **Valor enviado**
   clientName*                 nome completo
   clientType*                 PF
   clientDocument*             000\.000\.000-00
   clientMail*                 contato@exemplo.com
   clientPhone*                \(00\) 0000-0000
   clientSecondaryMail         contato2@exemplo.com
   clientBirthDate             0000-00-00
   billingStreet*              rua
   billingNumber*              00
   billingNeighbourhood*       bairro
   billingCity*                cidade
   billingState*               sigla do estado
   billingPostcode*            00000-000
   billingComplement           complemento
   billingResponsible*         nome completo
   deliveryStreet*             rua
   deliveryNumber*             00
   deliveryNeighbourhood*      bairro
   deliveryCity*               cidade
   deliveryState*              sigla do estado
   deliveryPostcode*           00000-000
   deliveryComplement          complemento
   deliveryResponsible*        nome completo
   cardOwnerName*              nome completo
   cardOwnerType*              PJ
   cardOwnerDocument*          00\.000\.000/0000-00
   cardOwnerPhone*             \(00\) 00000-0000
   cardFlag*                   bandeira
   cardNumber*                 0000\.0000\.0000\.0000
   cardMonth*                  00
   cardYear*                   0000
   cardCode*                   000
   antiFraud*                  1
   orderNumber*                11111
   value*                      50.00
   parcelNumber*               10
   capture*                    1
   ipAddress                   192.168.0.0
   os                          sistema operacional
   notificationUrl             url.exemplo.com
   browser                     navegador
   frequence                   monthly
   finalDate                   2020-01-01
   =========================== =================================

Header:

.. code:: javascript

   {
      "Account-Token":"*********************"
   }

**Resposta**

.. code:: javascript

   {
      "success": true,
      "data": [
         {
            "code": 123,
            "accountName": "CONTA DA TRANSAÇÃO",
            "payerName": "NOME COMPLETO",
            "orderNumber": "11111",
            "status": 41,
            "amount": "50.00",
            "fraudPoints": 0,
            "dateAnalysis": "06/11/2018 10:21:23",
            "message": "Transação realizada com sucesso",
            "recurrence": {
                  "active": true, //se a recorrência está ativa ou não
                  "amount": "50.00",
                  "carddate": "00/0000",
                  "cardname": "NOME COMPLETO",
                  "cardnumber": "000000******0000",
                  "code": 86, //código da recorrência, utilizado em requisições posteriores (importante salvar este código)
                  "datecreate": "06/12/2018 10:21:19", //data da criação da recorrência
                  "datelasttransaction": "06/12/2018 10:21:19", //data da execução da última transação da recorrência
                  "finaldate": "01/01/2020", //data final da recorrêcia
                  "frequence": "monthly"
            }
         }
      ]
   }


*********************************
Atualização de Recorrência
*********************************


:guilabel:`POST` ``https://api.branvopay.com/cartao/v2/recurrent/{recurrenceCode}``

**Endpoint utilizado para atualizar uma Recorrência**

.. important::
   Os dados atualizados nesta requisição são os da recorrência, ou seja, as transações já efetuadas não serão afetadas. As próximas transações passarão a ser realizadas
   com estes dados. Através deste método, é possível estender a validade de uma recorrência, pausar uma recorrência, alterar a sua frequência e o seu valor.

==========
Requisição
==========

.. table::
   :widths: 70 20 150

   +----------------------------+-----------------+------------------------------------------------------------+
   | **Header**                 | **Tipo**        | **Descrição**                                              |
   +----------------------------+-----------------+------------------------------------------------------------+
   | Account-Token*             | string          | | Token da conta na BranvoPay                              |
   +----------------------------+-----------------+------------------------------------------------------------+
   | **Body**                   | **Tipo**        | **Descrição**                                              |
   +============================+=================+============================================================+
   | frequence                  | string          | | Frequência de cobrança                                   | 
   |                            |                 | |                                                          |
   |                            |                 | | Valores aceitos:                                         |
   |                            |                 | | [monthly (mensal), bimonthly (cada 2 meses),             |
   |                            |                 | | quarterly (trimestral), semester (semestral),            |
   |                            |                 | | yearly (anual)]                                          |
   +----------------------------+-----------------+------------------------------------------------------------+
   | amount                     | decimal         | | Valor da recorrência.                                    |
   +----------------------------+-----------------+------------------------------------------------------------+
   | finalDate                  | string          | | Data final da recorrência. Ela será executada            |
   |                            |                 | | automaticamente até esta data.                           |
   +----------------------------+-----------------+------------------------------------------------------------+
   | active                     | int             | | Recorrência ativa ou desativada/pausada                  |
   |                            |                 | | Valores aceitos: [0 (ativa), 1 (desativada)]             |
   +----------------------------+-----------------+------------------------------------------------------------+
   
.. note::
   Parâmetros com asterisco ( * ) são obrigatórios

.. note::
   Nenhum dos parâmetros do corpo da requisição é obrigatório, porém é necessário informar pelo menos um deles para atualização.

========
Resposta
========

**Sucesso**

.. code:: javascript

   {
      "success":true,
      "data": {
         "active": bool,
         "amount": decimal,
         "carddate": string,
         "cardname": string,
         "cardnumber": string,
         "code": int,
         "datecreate": string,
         "datelasttransaction": string,
         "finaldate": string,
         "frequence": string
      }
   }

**Falha**

.. code:: javascript

   {
      "success": false,
      "errorCode": string,
      "errorMessage": string
   }

=======
Exemplo
=======

**Requisição**

.. table::
   :widths: 70 320

   =========================== =================================
   **Header**                  **Valor enviado**
   Account-Token               \********************************
   **Body**                    **Valor enviado**
   frequence                   yearly
   finalDate                   2021-01-01
   amount                      110.00
   active                      0
   =========================== =================================

Header:

.. code:: javascript

   {
      "Account-Token":"*********************"
   }

**Resposta**

.. code:: javascript

   {
      "success": true,
      "data": {
         "active": false, //se a recorrência está ativa ou não
         "amount": "110.00",
         "carddate": "00/0000",
         "cardname": "NOME COMPLETO",
         "cardnumber": "000000******0000",
         "code": 86,
         "datecreate": "06/12/2018 10:21:19", //data da criação da recorrência
         "datelasttransaction": "06/12/2018 10:21:19", //data da execução da última transação da recorrência
         "finaldate": "01/01/2021", //data final da recorrêcia
         "frequence": "yearly"
      }
   }

.. note::
   Este método não permite atualização dos dados de cartão de crédito utilizados para a cobrança recorrente. Para isso, é necessário desativar a recorrência e
   criar uma nova.


*********************************
Consulta de Recorrência
*********************************

:guilabel:`GET` ``https://api.branvopay.com/cartao/v2/recurrent/{recurrenceCode}``

**Endpoint utilizado para consultar os dados de uma Recorrência**

==========
Requisição
==========

.. table::
   :widths: 70 20 150

   +----------------------------+-----------------+------------------------------------------------------------+
   | Header                     | Tipo            | Descrição                                                  |
   +============================+=================+============================================================+
   | Account-Token*             | string          | Token da conta referente à transação                       |
   +----------------------------+-----------------+------------------------------------------------------------+

.. note::
   Parâmetros com asterisco ( * ) são obrigatórios

========
Resposta
========

**Sucesso**

.. code:: javascript

   {
      "success":true,
      "data": {
         "active": bool,
         "amount": decimal,
         "carddate": string,
         "cardname": string,
         "cardnumber": string,
         "code": int,
         "datecreate": string,
         "datelasttransaction": string,
         "finaldate": string,
         "frequence": string
      }
   }

**Falha**

.. code:: javascript

   {
      "success": false,
      "errorCode": string,
      "errorMessage": string
   }

=======
Exemplo
=======

**Requisição**

.. parsed-literal::
   **URL:** https://sandbox-api.branvopay.com/cartao/v2/recurrent/86

.. table::
   :widths: 70 320

   =========================== ===========================================================
   **Header**                  **Valor enviado**
   Account-Token               \********************************
   =========================== ===========================================================

**Resposta**

.. code:: javascript

   {
      "success": true,
      "data": {
         "active": false, //se a recorrência está ativa ou não
         "amount": "110.00",
         "carddate": "00/0000",
         "cardname": "NOME COMPLETO",
         "cardnumber": "000000******0000",
         "code": 86,
         "datecreate": "06/12/2018 10:21:19", //data da criação da recorrência
         "datelasttransaction": "06/12/2018 10:21:19", //data da execução da última transação da recorrência
         "finaldate": "01/01/2021", //data final da recorrêcia
         "frequence": "yearly"
      }
   }

******************************
Cartões de Crédito para teste
******************************

Fornecemos alguns números de cartões fictícios para que você consiga realizar testes em nosso ambiente sandbox. Cada número de cartão retornará um status de transação diferente, conforme a tabela abaixo.
**OBS.**: Todos os cartões de testes são da bandeira MasterCard.

.. important::
   Não envie os números de cartão abaixo para o ambiente de produção da API, pois as transações não serão aprovadas.

.. table::
   :widths: auto

   +-----------------------+------------------------+--------------------------------------------+
   | Status de Retorno     | Número do Cartão       | Mensagem de Retorno                        |
   +=======================+========================+============================================+
   | Autorizada            | | \5431 0906 3635 6361 | Operação realizada com sucesso             |
   |                       | | \5200 9031 9596 3964 |                                            |
   +-----------------------+------------------------+--------------------------------------------+
   | Não Autorizada        | \5320 0724 1834 5352   | Não Autorizada                             |
   +-----------------------+------------------------+--------------------------------------------+
   | Não Autorizada        | \5219 5462 1875 2333   | Cartão Expirado                            |
   +-----------------------+------------------------+--------------------------------------------+
   | Não Autorizada        | \5278 5739 0938 8415   | Cartão Bloqueado                           |
   +-----------------------+------------------------+--------------------------------------------+
   | Não Autorizada        | \5568 4860 6624 2136   | Time Out                                   |
   +-----------------------+------------------------+--------------------------------------------+
   | Não Autorizada        | \5173 1127 9089 8947   | Cartão Cancelado                           |
   +-----------------------+------------------------+--------------------------------------------+
   | Não Autorizada        | \5151 6834 4235 1788   | Problemas com o Cartão de Crédito          |
   +-----------------------+------------------------+--------------------------------------------+
   | Autorização Aleatória | \5556 4188 6566 3059   | Operação realizada com sucesso / Time Out  |
   +-----------------------+------------------------+--------------------------------------------+
   

Para testes com outras bandeiras, você pode utilizar um gerador de número de cartão de crédito online. Atente para o último dígito do número do cartão, pois é ele que determina qual será o retorno da transação.

