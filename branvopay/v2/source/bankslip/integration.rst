.. Branvo Pay documentation master file, created by
   sphinx-quickstart on Mon Nov  5 13:20:02 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Boleto:

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Cartão:

##############################
Boleto Bancário - Integrações
##############################

*******
Emissão
*******

:guilabel:`POST` ``https://api.branvopay.com/boleto/v2/emission``

**Endpoint utilizado para emitir um boleto bancário**

==========
Requisição
==========

.. table::
   :widths: 70 20 150

   +----------------------------+-----------------+------------------------------------------------------------+
   | **Header**                 | **Tipo**        | **Descrição**                                              |
   +----------------------------+-----------------+------------------------------------------------------------+
   | Account-Token*             | string          | | Token da conta na BranvoPay com 32 caracteres            |
   +----------------------------+-----------------+------------------------------------------------------------+
   | **Body**                   | **Tipo**        | **Descrição**                                              |
   +----------------------------+-----------------+------------------------------------------------------------+
   | clientName*                | string          | Nome completo do pagador com no até 40 caracteres          |
   +----------------------------+-----------------+------------------------------------------------------------+
   | clientType*                | string          | Tipo de pessoa do comprador. [PF, PJ]                      |
   +----------------------------+-----------------+------------------------------------------------------------+
   | clientDocument*            | string          | CPF/CNPJ do pagador                                        |
   +----------------------------+-----------------+------------------------------------------------------------+
   | clientMail*                | string          | E-mail do pagador                                          |
   +----------------------------+-----------------+------------------------------------------------------------+
   | clientSecondaryMail        | string          | E-mail secundário do pagador                               |
   +----------------------------+-----------------+------------------------------------------------------------+
   | clientPhone*               | string          | Telefone do Pagador com DDD                                |
   +----------------------------+-----------------+------------------------------------------------------------+
   | clientBirthDate            | string          | | Data de Nascimento do Pagador                            |
   |                            |                 | | Formato: YYYY-mm-dd                                      |
   +----------------------------+-----------------+------------------------------------------------------------+
   | billingStreet*             | string          | Rua do endereço do pagador com até 30 caracteres           |
   +----------------------------+-----------------+------------------------------------------------------------+
   | billingNumber*             | string          | Número do endereço do pagado com até 10 caracteres         |
   +----------------------------+-----------------+------------------------------------------------------------+
   | billingComplement          | string          | Complemento do endereço do pagador com até 30 caracteres   |
   +----------------------------+-----------------+------------------------------------------------------------+
   | billingNeighbourhood*      | string          | Bairro do endereço do pagador com até 20 caracteres        |
   +----------------------------+-----------------+------------------------------------------------------------+
   | billingCity*               | string          | Cidade do endereço do pagador com até 20 caracteres        |
   +----------------------------+-----------------+------------------------------------------------------------+
   | billingState*              | string          | Sigla do estado do endereço do pagador com 2 caracteres    |
   +----------------------------+-----------------+------------------------------------------------------------+
   | billingPostcode*           | string          | CEP do endereço do pagador                                 |
   +----------------------------+-----------------+------------------------------------------------------------+
   | description*               | string          | Descrição do boleto                                        |
   +----------------------------+-----------------+------------------------------------------------------------+
   | reference*                 | string          | | Número de referência do boleto (por exemplo,             |
   |                            |                 | | número do pedido).                                       |
   |                            |                 | | Este número é de controle do usuário BranvoPay           |
   +----------------------------+-----------------+------------------------------------------------------------+
   | amount*                    | decimal         | Valor do boleto                                            |
   +----------------------------+-----------------+------------------------------------------------------------+
   | dueDate*                   | string          | | Data de vencimento do boleto                             |
   |                            |                 | | Formato: YYYY-mm-dd                                      |
   +----------------------------+-----------------+------------------------------------------------------------+
   | installments*              | int             | Número de parcelas                                         |
   +----------------------------+-----------------+------------------------------------------------------------+
   | maxOverdueDays             | int             | | Permitir pagar em até quantos dias após o vencimento?    |
   |                            |                 | | Máx.: 30 dias                                            |
   |                            |                 | | Utilize 0 para não permitir pag. após o vencimento       |
   +----------------------------+-----------------+------------------------------------------------------------+
   | fine                       | decimal         | Multa, em R$                                               |
   +----------------------------+-----------------+------------------------------------------------------------+
   | interest                   | decimal         | Juros, em %                                                |
   +----------------------------+-----------------+------------------------------------------------------------+
   | abbreviate                 | int             | | 0 (false) ou 1 (true)                                    |
   |                            |                 | | Ativar a abreviação automática dos dados enviados?       |
   |                            |                 | | Caso essa opção seja acionada, a api não retornará erros |
   |                            |                 | | referentes à limitação de tamanho dos campos, abreviando |
   |                            |                 | | automaticamente os dados quando o limite de caracteres   |
   |                            |                 | | de algum campo for ultrapassado                          |
   |                            |                 | | Ex: Avenida Governador Pedro Gomes Terceiro              |
   |                            |                 | | É abreviado para Av Gov Pedro Gomes III                  |
   +----------------------------+-----------------+------------------------------------------------------------+
   | logo                       | string          | | URL completa do logotipo da empresa                      |
   |                            |                 | | É necessário estar no formato http:// ...                |
   |                            |                 | | Caso o link seja inválido, o boleto será gerado          |
   |                            |                 | | normalmente sem a imagem                                 |
   |                            |                 | | As extensões aceitas são PNG, JPG e GIF                  |
   +----------------------------+-----------------+------------------------------------------------------------+
   | notificationUrl            | string          | | URL de notificação do boleto                             |
   |                            |                 | | Esta URL será acionada assim que o boleto for            |
   |                            |                 | | atualizado conforme retorno bancário                     |
   +----------------------------+-----------------+------------------------------------------------------------+
   | showTop                    | int             | | Mostrar topo do boleto com instruções de impressão padrão|
   |                            |                 | | Enviar 0 para não mostrar                                |
   |                            |                 | | Padrão será 1 (mostrará o topo caso não enviar nada)     |
   +----------------------------+-----------------+------------------------------------------------------------+

.. note::
   Parâmetros com asterisco ( * ) são obrigatórios

========
Resposta
========

**Sucesso**

.. code:: javascript

   {
      "success": true,
      "data":{
         "charges":[
            {
               "code": string,
               "reference": string,
               "amount": decimal,
               "dueDate": string,
               "link": string,
               "installmentLink": string
            }
         ]
      }
   }

**Falha**

.. code:: javascript

     {
         "success": false,
         "errorCode": string,
         "errorMessage": string
     }

=======
Exemplo
=======

**Requisição**

.. table::
    :widths: 70 250

    +----------------------------+-----------------------------------------------------------------------------+
    | **Header**                 |                                                                             |
    +----------------------------+-----------------------------------------------------------------------------+
    | Account-Token*             | \*******************                                                        |
    +----------------------------+-----------------------------------------------------------------------------+
    | **Body**                   |                                                                             |
    +----------------------------+-----------------------------------------------------------------------------+
    | clientName*                | nome completo                                                               |
    +----------------------------+-----------------------------------------------------------------------------+
    | clientDocument*            | 000.000.000-00                                                              |
    +----------------------------+-----------------------------------------------------------------------------+
    | clientMail*                | contato@exemplo.com                                                         |
    +----------------------------+-----------------------------------------------------------------------------+
    | clientSecondaryMail        | contato2@exemplo.com                                                        |
    +----------------------------+-----------------------------------------------------------------------------+
    | clientPhone*               | 00 0000-0000                                                                |
    +----------------------------+-----------------------------------------------------------------------------+
    | clientBirthDate            | 0000-00-00                                                                  |
    +----------------------------+-----------------------------------------------------------------------------+
    | billingStreet*             | Rua                                                                         |
    +----------------------------+-----------------------------------------------------------------------------+
    | billingNumber*             | 00                                                                          |
    +----------------------------+-----------------------------------------------------------------------------+
    | billingComplement          | complemento                                                                 |
    +----------------------------+-----------------------------------------------------------------------------+
    | billingNeighborhood*       | bairro                                                                      |
    +----------------------------+-----------------------------------------------------------------------------+
    | billingCity*               | cidade                                                                      |
    +----------------------------+-----------------------------------------------------------------------------+
    | billingState*              | sigla do estado                                                             |
    +----------------------------+-----------------------------------------------------------------------------+
    | billingPostcode*           | 00000-000                                                                   |
    +----------------------------+-----------------------------------------------------------------------------+
    | description*               | descrição da compra                                                         |
    +----------------------------+-----------------------------------------------------------------------------+
    | reference*                 | 11111                                                                       |
    +----------------------------+-----------------------------------------------------------------------------+
    | amount*                    | 50.00                                                                       |
    +----------------------------+-----------------------------------------------------------------------------+
    | dueDate*                   | 0000-00-00                                                                  |
    +----------------------------+-----------------------------------------------------------------------------+
    | installments*              | 1                                                                           |
    +----------------------------+-----------------------------------------------------------------------------+
    | maxOverdueDays             | 5                                                                           |
    +----------------------------+-----------------------------------------------------------------------------+
    | fine                       | 1.99                                                                        |
    +----------------------------+-----------------------------------------------------------------------------+
    | interest                   | 0.99                                                                        |
    +----------------------------+-----------------------------------------------------------------------------+
    | notificationUrl            | www.url.exemplo.com                                                         |
    +----------------------------+-----------------------------------------------------------------------------+

**Resposta**

.. code:: javascript

  {
      "success": true,
      "data": {
          "charges": [
              {
                  "code": "10000000000",
                  "reference": "11111",
                  "amount": "50.00",
                  "dueDate": "00/00/0000",
                  "link": "https://sandbox-api.branvopay.com/boleto/v2/printall/2aef3a56272a816ebf7d1a49b207b5e2",
                  "installmentLink": "https://sandbox-api.branvopay.com/boleto/v2/print/d1980d55fc05a3052655f61a6ac29e03"
              }
          ]
      }
  }

******************
Split de Pagamento
******************

:guilabel:`POST` ``https://api.branvopay.com/boleto/v2/emission``


O Split de Pagamentos é um sistema que realiza as divisões dos pagamentos e taxas entre duas ou mais contas Branvo Pay, realizando uma única cobrança do seu
cliente e simplificando o gerenciamento de recebíveis. Você pode informar quantas contas forem necessárias para dividir os pagamentos, especificando a quantia
que cada conta deve receber, em forma de valor líquido ou porcentagem.

==========
Requisição
==========

.. important::
   O Split de Pagamentos por boleto requer o envio de todos os parâmetros da emissão de um boleto comum, juntamente com os parâmetros da tabela abaixo:

.. table::
   :widths: 70 20 150

   +----------------------------+-----------------+------------------------------------------------------------+
   | Body                       | Tipo            | Descrição                                                  |
   +============================+=================+============================================================+
   | ...                        | ...             | | ...                                                      |
   +----------------------------+-----------------+------------------------------------------------------------+
   | split*                     | int             | | 0 (false) ou 1 (true)                                    |
   |                            |                 | | Transação será feita por split de pagamentos             |
   |                            |                 | | Se for 1, todos os parâmetros abaixo tornam-se           |
   |                            |                 | | obrigatórios                                             |
   +----------------------------+-----------------+------------------------------------------------------------+
   | splitType*                 | string          | | Os valores enviados serão em formato valor ou            |
   |                            |                 | | porcentagem                                              |
   |                            |                 | | Valores aceitos: [val (valor), perc (porcentagem)]       |
   +----------------------------+-----------------+------------------------------------------------------------+
   | splitAmount*               | array           | | Array com valores a serem repartidos                     |
   |                            |                 | | Ex.: splitAmount[0] = 100.00, splitAmount[1] = 50.00...  |
   +----------------------------+-----------------+------------------------------------------------------------+
   | splitToken*                | array           | | Array com tokens das contas que irão receber os valores  |
   |                            |                 | | **OBS.:** Respeitar a ordem do parâmetro splitAmount,    |
   |                            |                 | | Ex.: Se splitAmount[0] = 100.00 e splitToken[0] = 123    |
   |                            |                 | | a conta referente ao token 123 receberá R$ 100,00,       |
   |                            |                 | | e assim por diante                                       |
   +----------------------------+-----------------+------------------------------------------------------------+

.. note::
   Parâmetros com asterisco ( * ) são obrigatórios

========
Resposta
========

**Sucesso**

.. code:: javascript

   {
      "success": bool,
      "data":{
         "charges":[
            {
               "code": string,
               "reference": string,
               "amount": decimal,
               "dueDate": string,
               "link": string,
               "installmentLink": string
            }
         ]
      }
   }

**Falha**

.. code:: javascript

     {
         "success": false,
         "errorCode": string,
         "errorMessage": string
     }

=======
Exemplo
=======

**Requisição**

.. table::
  :widths: 40 250

  +---------------------------+------------------------------------------------------------------------+
  | **Body**                  | **Valor enviado**                                                      |
  +---------------------------+------------------------------------------------------------------------+
  | ...                       | ...                                                                    |
  +---------------------------+------------------------------------------------------------------------+
  | split                     | 1                                                                      |
  +---------------------------+------------------------------------------------------------------------+
  | splitType                 | val                                                                    |
  +---------------------------+------------------------------------------------------------------------+
  | splitAmount               | [25.00, 25.00]                                                         |
  +---------------------------+------------------------------------------------------------------------+
  | splitToken                | [\********************************, \********************************] |
  +---------------------------+------------------------------------------------------------------------+

**Resposta**

.. code::

  {
      "success": true,
      "data": {
          "charges": [
              {
                  "code": "10000000000",
                  "reference": "11111",
                  "amount": "50.00",
                  "dueDate": "00/00/0000",
                  "link": "https://sandbox-api.branvopay.com/boleto/v2/printall/a3035079f09e670ada2c5dbb3ae6d436",
                  "installmentLink": "https://sandbox-api.branvopay.com/boleto/v2/print/da6c875d8c97a31859044fea83b57903"
              }
          ]
      }
  }

************
Cancelamento
************

:guilabel:`PUT` ``https://api.branvopay.com/boleto/v2/cancel/{code}``

**Realiza a solicitação de cancelamento do boleto, pelo seu número**

.. attention::

   Esta operação **NÃO** garante a baixa do boleto, ela apenas é
   confirmada mediante aprovação e retorno bancário. Ou seja, será solicitada
   a baixa do boleto, o banco irá analisar, processar a baixa e retornar, e através
   da sua URL de notificação ele deverá ser efetivamente cancelado pela
   aplicação do usuário BranvoPay

==========
Requisição
==========

.. table::
   :widths: 70 20 150

   +----------------------------+-----------------+------------------------------------------------------------+
   | Header                     | Tipo            | Descrição                                                  |
   +============================+=================+============================================================+
   | Account-Token*             | string          | Token da conta referente à transação com 32 caracteres     |
   +----------------------------+-----------------+------------------------------------------------------------+

.. note::
   Parâmetros com asterisco ( * ) são obrigatórios

========
Resposta
========

**Sucesso**

.. code:: javascript

   {
       "success": bool,
       "data": {
           "charges": [
               {
                   "code": string,
                   "amount": decimal,
                   "dueDate": string,
                   "cancel": bool,
                   "status": string
               }
           ]
       }
   }

**Falha**

.. code:: javascript

     {
         "success": false,
         "errorCode": string,
         "errorMessage": string
     }

=======
Exemplo
=======

**Requisição**

.. parsed-literal::
   **URL:** https://api.branvopay.com/boleto/v2/cancel/10000000000

.. table::
   :widths: 70 320

   =========================== ===========================================================
   **Header**                  **Valor enviado**
   Account-Token               \********************************
   =========================== ===========================================================

**Resposta**

.. code:: javascript

   {
       "success": true,
       "data": {
           "charges": [
               {
                   "code": "10000000000",
                   "reference": "11111",
                   "amount": "50.00",
                   "dueDate": "00/00/0000",
                   "cancel": true,
                   "status": "Solicitação de Baixa de Título"
               }
           ]
       }
   }

********
Consulta
********

:guilabel:`GET` ``https://api.branvopay.com/boleto/v2/consult/{code}``

**Realiza a consulta de status do boleto, pelo seu número**

==========
Requisição
==========

.. table::
   :widths: 70 20 170

   +----------------------------+-----------------+------------------------------------------------------------+
   | Header                     | Tipo            | Descrição                                                  |
   +============================+=================+============================================================+
   | Account-Token*             | string          | Token da conta referente à transação com 32 caracteres     |
   +----------------------------+-----------------+------------------------------------------------------------+

.. note::
   Parâmetros com asterisco ( * ) são obrigatórios

========
Resposta
========

**Sucesso**

.. code:: javascript

   {
       "success": bool,
       "data": {
           "charges": [
               {
                   "code": string,
                   "amount": decimal,
                   "dueDate": string,
                   "cancel": bool,
                   "status": string
               }
           ]
       }
   }

**Falha**

.. code:: javascript

     {
         "success": false,
         "errorCode": string,
         "errorMessage": string
     }

=======
Exemplo
=======

**Requisição**

.. parsed-literal::
   **URL:** https://api.branvopay.com/boleto/v2/consult/10000000000

.. table::
   :widths: 70 320

   =========================== ===========================================================
   **Header**                  **Valor enviado**
   Account-Token               \********************************
   =========================== ===========================================================

**Resposta**

.. code:: javascript

   {
       "success": true,
       "data": {
           "charges": [
               {
                   "code": "10000000000",
                   "reference": "11111",
                   "amount": "50.00",
                   "dueDate": "00/00/0000",
                   "cancel": true,
                   "status": "Solicitação de Baixa de Título"
               }
           ]
       }
   }