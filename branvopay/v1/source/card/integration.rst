.. Branvo Domain Whois documentation master file, created by
   sphinx-quickstart on Mon Nov  5 13:20:02 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Boleto:

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Cartão:

##########
Integração
##########

**************
Nova transação
**************

:guilabel:`GET` ``https://gateway.branvo.com/api/cartao/v1/new/``

**URL utilizada para criar uma nova transação de cartão de crédito**

==========
Requisição
==========

.. table::
   :widths: auto

   +----------------------------+-----------------+------------------------------------------------------------+
   | Parâmetro                  | Tipo            | Descrição                                                  |
   +============================+=================+============================================================+
   | cardName*                  | string          | | Nome do portador do cartão                               |
   +----------------------------+-----------------+------------------------------------------------------------+
   | cardNumber*                | string          | | Número do cartão                                         |
   +----------------------------+-----------------+------------------------------------------------------------+
   | cardMonth*                 | string          | | Mês de vencimento do cartão                              |
   |                            |                 | | Formato: MM. Ex.: 01, 02, 10, 12                         |
   +----------------------------+-----------------+------------------------------------------------------------+
   | cardYear*                  | string          | | Ano de vencimento do cartão                              |
   |                            |                 | | Formato: YY ou YYYY. Ex.: 22, 2022                       |
   +----------------------------+-----------------+------------------------------------------------------------+
   | cardCode*                  | string          | | Código de segurança do cartão                            |
   +----------------------------+-----------------+------------------------------------------------------------+
   | cardFlag*                  | string          | | Bandeira do cartão                                       |
   |                            |                 | | Valores aceitos: [mastercard, visa, discover, elo, amex  |
   |                            |                 | | jcb, aura]                                               |
   +----------------------------+-----------------+------------------------------------------------------------+
   | cardPhone*                 | string          | | Telefone do titular do cartão                            |
   +----------------------------+-----------------+------------------------------------------------------------+
   | cardDocument*              | string          | | Documento (CPF) do titular do cartão                     |
   +----------------------------+-----------------+------------------------------------------------------------+
   | notificationUrl            | string          | | URL para notificação de atualização da transação         |
   |                            |                 | | Ver sessão “URL de notificação”                          |
   +----------------------------+-----------------+------------------------------------------------------------+
   | clientName*                | string          | | Nome do comprador                                        |
   +----------------------------+-----------------+------------------------------------------------------------+
   | clientDocument*            | string          | | Documento (CPF) do comprador                             |
   +----------------------------+-----------------+------------------------------------------------------------+
   | personType*                | string          | | Tipo de pessoa do comprador. Valores aceitos [PF, PJ]    |
   +----------------------------+-----------------+------------------------------------------------------------+
   | mail*                      | string          | | E-mail do comprador                                      |
   +----------------------------+-----------------+------------------------------------------------------------+
   | phone*                     | string          | | Telefone do comprador                                    |
   +----------------------------+-----------------+------------------------------------------------------------+
   | orderNumber*               | int             | | Número do pedido                                         |
   +----------------------------+-----------------+------------------------------------------------------------+
   | value*                     | decimal         | | Valor total do pedido                                    |
   +----------------------------+-----------------+------------------------------------------------------------+
   | parcelNumber*              | int             | | Número de parcelas. Valores aceitos: [1-12]              |
   |                            |                 | | Utilizar 1 para à vista                                  |
   +----------------------------+-----------------+------------------------------------------------------------+
   | token*                     | string          | | Token da conta na BranvoPay                              |
   +----------------------------+-----------------+------------------------------------------------------------+
   | ipAddress                  | string          | | Endereço IP do comprador                                 |
   +----------------------------+-----------------+------------------------------------------------------------+
   | browser                    | string          | | Navegador utilizado pelo comprador                       |
   +----------------------------+-----------------+------------------------------------------------------------+
   | os                         | string          | | Sistema operacional utilizado pelo comprador             |
   +----------------------------+-----------------+------------------------------------------------------------+
   | street*                    | string          | | Rua do endereço de entrega                               |
   +----------------------------+-----------------+------------------------------------------------------------+
   | number*                    | string          | | Número do endereço de entrega                            |
   +----------------------------+-----------------+------------------------------------------------------------+
   | neighbourhood*             | string          | | Bairro do endereço de entrega                            |
   +----------------------------+-----------------+------------------------------------------------------------+
   | complement                 | string          | | Complemento do endereço de entrega                       |
   +----------------------------+-----------------+------------------------------------------------------------+
   | responsible*               | string          | | Responsável por receber                                  |
   +----------------------------+-----------------+------------------------------------------------------------+
   | city*                      | string          | | Cidade do endereço de entrega                            |
   +----------------------------+-----------------+------------------------------------------------------------+
   | state*                     | string          | | Estado do endereço de entrega                            |
   |                            |                 | | Formato: [EE]. Ex.: RS, SC, SP                           |
   +----------------------------+-----------------+------------------------------------------------------------+
   | cep*                       | string          | | CEP do endereço de entrega                               |
   |                            |                 | |  Formato: [CCCCCCC]. Ex.: 95720000                       |
   +----------------------------+-----------------+------------------------------------------------------------+
   | billingStreet*             | string          | | Rua do endereço de cobrança do cliente                   |
   +----------------------------+-----------------+------------------------------------------------------------+
   | billingNumber*             | string          | | Número do endereço de cobrança do cliente                |
   +----------------------------+-----------------+------------------------------------------------------------+
   | billingNeighbourhood*      | string          | | Bairro do endereço de cobrança do cliente                |
   +----------------------------+-----------------+------------------------------------------------------------+
   | billingComplement          | string          | | Complemento do endereço de cobrança do cliente           |
   +----------------------------+-----------------+------------------------------------------------------------+
   | billingResponsible*        | string          | | Responsável do endereço de cobrança do cliente           |
   +----------------------------+-----------------+------------------------------------------------------------+
   | billingCity*               | string          | | Cidade do endereço de cobrança do cliente                |
   +----------------------------+-----------------+------------------------------------------------------------+
   | billingState*              | string          | | Estado do endereço de cobrança do cliente                |
   +----------------------------+-----------------+------------------------------------------------------------+
   | billingCep*                | string          | | CEP do endereço de cobrança do cliente                   |
   +----------------------------+-----------------+------------------------------------------------------------+
   | antiFraud*                 | int             | | 0 (false) ou 1 (true)                                    |
   |                            |                 | | Passar por análise automática de antifraude?             |
   |                            |                 | | OBS.: Caso a transação passe da análise com 0 ponto,     |
   |                            |                 | | ela irá automaticamente para a fila de processamento     |
   +----------------------------+-----------------+------------------------------------------------------------+
   | capture*                   | int             | | 0 (false) ou 1 (true)                                    |
   |                            |                 | | Realizar a captura automática?                           |
   |                            |                 | | Se for true, a transação será descontada do cliente no   |
   |                            |                 | | momento da transação, caso for aprovada                  |
   |                            |                 | | OBS.: A captura automática só poderá ser ativada caso    |
   |                            |                 | | não houver análise antifraude                            |
   +----------------------------+-----------------+------------------------------------------------------------+

.. note::
   Parâmetros com asterisco ( * ) são obrigatórios

========
Resposta
========

**Sucesso**

.. code:: javascript

   {
      "success": true,
      "data": {
         "code": int
         "orderNumber": string
         "status": int
         "amount": decimal
         "fraudPoints": int
         "dateAnalysis": string
         "queueOrder": int
         "retry": bool
         "message": string
         "split": bool
      }
   }

**Falha**

.. code:: javascript

   {
      "success": false,
      "data": {
         "message": "{mensagem de erro correspondente}"
      }
   }

=======
Exemplo
=======

**Requisição**

.. parsed-literal::

    `https://sandbox-gateway.branvo.com.br/api/cartao/v1/new/?cardName=TESTECOMPRA&cardNumber=5360304408437050&cardMonth=04&cardYear=2020&cardCode=123&cardFlag=mastercard&cardPhone=54999999999&cardDocument=34469129089&notificationUrl=http://url.de.retorno.com/atualizar-boleto/&clientName=TESTECOMPRA&clientDocument=34469129089&personType=PF&mail=teste@branvo.com&phone=54999999999&orderNumber=1899000054&value=150.00&parcelNumber=1&token=77F8ECCA8E9CA66FF5DB48812CD07544&ipAddress=10.0.0.1&browser=GoogleChrome&os=windows&street=RuaTeste&number=50&neighbourhood=BairroTeste&responsible=TesteCompra&city=SaoPaulo&state=SP&cep=95700000&billingStreet=RuaTeste&billingNumber=50&billingNeighbourhood=BairroTeste&billingResponsible=TesteCompra&billingCity=SaoPaulo&billingState=SP&billingCep=95700000&antiFraud=0&capture=1&split=0`

**Resposta**

.. code:: javascript

   {
      "success":true,
      "data":{
         "code":"0001",
         "orderNumber":"1899000054",
         "status":41,
         "amount":150.00,
         "fraudPoints":0,
         "dateAnalysis":"2018-08-01 11:00:00",
         "queueOrder":1,
         "retry":false,
         "message":"Operation successful",
         "split":false
      }
   }

************************
Nova transação com split
************************

:guilabel:`GET` ``https://gateway.branvo.com/api/cartao/v1/new/``

**O que é split de pagamento?**

É um sistema que realiza as divisões de pagamentos aos seus fornecedores, realizando uma única cobrança do seu
cliente e simplificando o gerenciamento de recebíveis, dividindo o valor entre uma conta principal e outras contas secundárias.

==========
Requisição
==========

.. table::
   :widths: auto

   +----------------------------+-----------------+------------------------------------------------------------+
   | Parâmetro                  | Tipo            | Descrição                                                  |
   +============================+=================+============================================================+
   | split                      | int             | | 0 (false) ou 1 (true)                                    |
   |                            |                 | | Transação será feita por split de pagamentos             |
   |                            |                 | | Se for 1, todos os parâmetros abaixo tornam-se           |
   |                            |                 | | obrigatórios                                             |
   +----------------------------+-----------------+------------------------------------------------------------+
   | splitType                  | string          | | Os valores enviados serão em formato valor ou            |
   |                            |                 | | porcentagem                                              |
   |                            |                 | | Valores aceitos: [val (valor), perc (porcentagem)]       |
   +----------------------------+-----------------+------------------------------------------------------------+
   | splitAmount                | array           | | Array com valores a serem repartidos                     |
   |                            |                 | | Ex.: splitAmount[0] = 100.00, splitAmount[1] = 50.00...  |
   +----------------------------+-----------------+------------------------------------------------------------+
   |  splitToken                | array           | | Array com tokens das contas que irão receber os valores  |
   |                            |                 | | **OBS.:** Respeitar a ordem do parâmetro splitAmount,    |
   |                            |                 | | Ex.: Se splitAmount[0] = 100.00 e splitToken[0] = 123    |
   |                            |                 | | a conta referente ao token 123 receberá R$ 100,00,       |
   |                            |                 | | e assim por diante                                       |
   +----------------------------+-----------------+------------------------------------------------------------+

========
Resposta
========

**Sucesso**

.. code:: javascript

   {
      "success": true,
      "data": {
         "code": int
         "orderNumber": string
         "status": int
         "amount": decimal
         "fraudPoints": int
         "dateAnalysis": string
         "queueOrder": int
         "retry": bool
         "message": string
         "split": bool
      }
   }

**Falha**

.. code:: javascript

   {
      "success": false,
      "data": {
         "message": "{mensagem de erro correspondente}"
      }
   }



=======
Exemplo
=======

**Requisição**

.. parsed-literal::

    `https://sandbox-gateway.branvo.com.br/api/cartao/v1/new/?cardName=TESTECOMPRA&cardNumber=5360304408437050&cardMonth=04&cardYear=2020&cardCode=123&cardFlag=mastercard&cardPhone=54999999999&cardDocument=34469129089&notificationUrl=http://url.de.retorno.com/atualizar-boleto/&clientName=TESTECOMPRA&clientDocument=34469129089&personType=PF&mail=teste@branvo.com&phone=54999999999&orderNumber=1899000054&value=150.00&parcelNumber=1&token=77F8ECCA8E9CA66FF5DB48812CD07544&ipAddress=10.0.0.1&browser=GoogleChrome&os=windows&street=RuaTeste&number=50&neighbourhood=BairroTeste&responsible=TesteCompra&city=SaoPaulo&state=SP&cep=95700000&billingStreet=RuaTeste&billingNumber=50&billingNeighbourhood=BairroTeste&billingResponsible=TesteCompra&billingCity=SaoPaulo&billingState=SP&billingCep=95700000&antiFraud=0&capture=1&split=1&splitType=val&splitToken[]=77F8ECCA8E9CA66FF5DB48812CD07544&splitToken[]=321D27C059A410EF0BD2C21A136CB29A&splitAmount[]=100.00&splitAmount[]=50.00`

**Resposta**

.. code:: javascript

   {
      "success":true,
      "data":{
         "code":"0001",
         "orderNumber":"1899000054",
         "status":41,
         "am ount":150.00,
         "fraudPoints":0,
         "dateAnalysis":"2018-08-01 11:00:00",
         "queueOrder":1,
         "retry":false,
         "me ssage":"Operation successful",
         "split":true
      }
   }



************
Cancelamento
************

:guilabel:`GET` ``https://gateway.branvo.com/api/cartao/v1/cancel/{orderNumber}/``

**Realiza o cancelamento e estorno da operação através do parâmetro orderNumber recebido, sendo que orderNumber é o número do pedido da loja.**

.. attention::

    Pode ser utilizado o mesmo orderNumber enviado para criar a transação.
    Necessário enviar via GET o token da conta referente à transação

==========
Requisição
==========

.. table::
   :widths: auto

   +----------------------------+-----------------+------------------------------------------------------------+
   | Parâmetro                  | Tipo            | Descrição                                                  |
   +============================+=================+============================================================+
   | token*                     | string          | Token da conta referente à transação                       |
   +----------------------------+-----------------+------------------------------------------------------------+

.. note::
   Parâmetros com asterisco ( * ) são obrigatórios

========
Resposta
========

**Sucesso**

.. code:: javascript

   {
      "success": true,
      "data": {
         "code": int
         "orderNumber": string
         "status": int
         "amount": decimal
         "fraudPoints": int
         "dateAnalysis": string
         "queueOrder": int
         "retry": bool
         "message": string
         "split": bool
      }
   }

**Falha**

.. code:: javascript

   {
      "success": false,
      "data": {
         "message": "{mensagem de erro correspondente}"
      }
   }

=======
Exemplo
=======

**Requisição**

.. parsed-literal::

    `https://sandboxgateway.branvo.com.br/api/cartao/v1/cancel/1899000054/?token=77F8ECCA8E9CA66FF5DB48812CD07544`

**Resposta**

.. code:: javascript

   {
      "success":true,
      "data":{
         "code":"0001",
         "orderNumber":"1899000054",
         "status":41,
         "amount":150.00,
         "fraudPoints":0,
         "dateAnalysis":"2018-08-01 11:00:00",
         "queueOrder":1,
         "retry":false,
         "message":"Operation successful"
      }
   }

******
Testes
******

========
Usuários
========

Para realizar os testes em ambiente sandbox, pode ser usado o seguinte usuário (token) de testes:

.. table::
   :widths: auto

   +----------------------------+----------------------------------+
   | Usuário                    | Token                            |
   +============================+==================================+
   | Test Main Account          | 77F8ECCA8E9CA66FF5DB48812CD07544 |
   +----------------------------+----------------------------------+

Para realização de testes de split de pagamentos, pode ser utilizado, juntamente com o token de testes acima, o seguinte token:

.. table::
   :widths: auto

   +----------------------------+----------------------------------+
   | Usuário                    | Token                            |
   +============================+==================================+
   | Test Split  Account        | 321D27C059A410EF0BD2C21A136CB29A |
   +----------------------------+----------------------------------+

==================
Cartões de Crédito
==================

.. table::
   :widths: auto

   +-----------------------+------------------------+--------------------------------------------+
   | Status de Retorno     | Final do Cartão        | Mensagem de Retorno                        |
   +=======================+========================+============================================+
   | Autorizada            | | \****.****.****.***1 | Operação realizada com sucesso             |
   |                       | | \****.****.****.***4 |                                            |
   +-----------------------+------------------------+--------------------------------------------+
   | Não Autorizada        | \****.****.****.***2   | Não Autorizada                             |
   +-----------------------+------------------------+--------------------------------------------+
   | Não Autorizada        | \****.****.****.***3   | Cartão Expirado                            |
   +-----------------------+------------------------+--------------------------------------------+
   | Não Autorizada        | \****.****.****.***5   | Cartão Bloqueado                           |
   +-----------------------+------------------------+--------------------------------------------+
   | Não Autorizada        | \****.****.****.***6   | Time Out                                   |
   +-----------------------+------------------------+--------------------------------------------+
   | Não Autorizada        | \****.****.****.***7   | Cartão Cancelado                           |
   +-----------------------+------------------------+--------------------------------------------+
   | Não Autorizada        | \****.****.****.***8   | Problemas com o Cartão de Crédito          |
   +-----------------------+------------------------+--------------------------------------------+
   | Autorização Aleatória | \****.****.****.***9   | Operação realizada com sucesso / Time Out  |
   +-----------------------+------------------------+--------------------------------------------+

.. important::

   A tabela mostra apenas os finais de cartão necessários. Podem ser informados
   quaisquer números de cartão, desde que sejam válidos e com estes finais.
