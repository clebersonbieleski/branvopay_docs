.. Branvo Domain Whois documentation master file, created by
   sphinx-quickstart on Mon Nov  5 13:20:02 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Boleto:

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Cartão:
   
##########
Integração
##########

*******
Emissão
*******

:guilabel:`GET` ``https://gateway.branvo.com/api/boleto/v1/emission/``

**URL utilizada para emitir um boleto bancário**

==========
Requisição
==========

.. table::
   :widths: auto

   +----------------------------+-----------------+------------------------------------------------------------+
   | Parâmetro                  | Tipo            | Descrição                                                  |
   +============================+=================+============================================================+
   | token*                     | string          | Token da conta do usuário BranvoPay                        |
   +----------------------------+-----------------+------------------------------------------------------------+
   | description*               | string          | Descrição do boleto                                        |
   +----------------------------+-----------------+------------------------------------------------------------+
   | reference*                 | string          | | Número de referência do boleto (por exemplo,             |
   |                            |                 | | número do pedido).                                       |
   |                            |                 | | Este número é de controle do usuário BranvoPay           |
   +----------------------------+-----------------+------------------------------------------------------------+
   | amount*                    | decimal         | Valor do boleto                                            |
   +----------------------------+-----------------+------------------------------------------------------------+
   | dueDate*                   | string          | | Data de vencimento do boleto                             |
   |                            |                 | | Formato: dd/mm/YYYY                                      |
   +----------------------------+-----------------+------------------------------------------------------------+
   | installments*              | int             | Número de parcelas                                         |
   +----------------------------+-----------------+------------------------------------------------------------+
   | maxOverdueDays*            | int             | | Permitir pagar em até quantos dias após o vencimento?    |
   |                            |                 | | Máx.: 30 dias                                            |
   |                            |                 | | Utilize 0 para não permitir pagamento após o vencimento  |
   +----------------------------+-----------------+------------------------------------------------------------+
   | fine*                      | decimal         | Multa, em R$                                               |
   +----------------------------+-----------------+------------------------------------------------------------+
   | interest*                  | decimal         | Juros, em %                                                |
   +----------------------------+-----------------+------------------------------------------------------------+
   | discountAmount*            | decimal         | Valor de desconto                                          |
   +----------------------------+-----------------+------------------------------------------------------------+
   | discountDays*              | int             | Dias de desconto                                           |
   +----------------------------+-----------------+------------------------------------------------------------+
   | payerName*                 | string          | Nome do pagador                                            |
   +----------------------------+-----------------+------------------------------------------------------------+
   | payerCpf*                  | string          | CPF do pagador                                             |
   +----------------------------+-----------------+------------------------------------------------------------+
   | payerEmail*                | string          | E-mail do pagador                                          |
   +----------------------------+-----------------+------------------------------------------------------------+
   | payerSecondaryEmail*       | string          | E-mail secundário do pagador                               |
   +----------------------------+-----------------+------------------------------------------------------------+
   | payerPhone*                | string          | Telefone do Pagador                                        |
   +----------------------------+-----------------+------------------------------------------------------------+
   | payerBirthDate*            | string          | | Data de Nascimento do Pagador                            |
   |                            |                 | | Formato: dd/mm/YYYY                                      |
   +----------------------------+-----------------+------------------------------------------------------------+
   | billingAddressStreet*      | string          | Rua do endereço do pagador                                 |
   +----------------------------+-----------------+------------------------------------------------------------+
   | billingAddressNumber       | string          | Número do endereço do pagado                               |
   +----------------------------+-----------------+------------------------------------------------------------+
   | billingAddressComplement   | string          | Complemento do endereço do pagador                         |
   +----------------------------+-----------------+------------------------------------------------------------+
   | billingAddressNeighborhood | string          | Bairro do endereço do pagador                              |
   +----------------------------+-----------------+------------------------------------------------------------+
   | billingAddressCity*        | string          | Cidade do endereço do pagador                              |
   +----------------------------+-----------------+------------------------------------------------------------+
   | billingAddressState*       | string          | Estado do endereço do pagador                              |
   +----------------------------+-----------------+------------------------------------------------------------+
   | billingAddressPostcode*    | string          | CEP do endereço do pagador                                 |
   +----------------------------+-----------------+------------------------------------------------------------+
   | notificationUrl*           | string          | | URL de notificação do boleto                             |
   |                            |                 | | Esta URL será acionada assim que o boleto for atualizado |
   |                            |                 | | conforme retorno bancário                                |
   +----------------------------+-----------------+------------------------------------------------------------+

.. note::
   Parâmetros com asterisco ( * ) são obrigatórios

========
Resposta
========

**Sucesso**

.. code:: javascript

   {
      "success": bool,
      "data":{
         "charges":[
            {
               "code": string,
               "amount": decimal,
               "dueDate": string,
               "link": string,
               "installmentLink": string
            }
         ]
      }
   }

**Falha**

.. code:: javascript

   {
      "success": false,
      "data": {
         "message": "{mensagem de erro correspondente}"
      }
   }

=======
Exemplo
=======

**Requisição**

.. parsed-literal::

    `https://sandboxgateway.branvo.com.br/api/boleto/v1/emission/?token=D5DCEHY67T159B3282A1950EF221535F&description=Boletoteste&reference=1876000031&amount=150.00&dueDate=10/10/2018&installments=1&maxOverdueDays=0&fine=0.00&interest=0.00&discountAmount=0.00&discountDays=0.00&payerName=Testepagador&payerCpf=48824235700&payerEmail=tecnico@branvo.com&payerSecondaryEmail=tecnico@branvo.com&payerPhone=5499999999&payerBirthDate=01/04/1990&billingAddressStreet=Ruateste&billingAddressNumber=100&billingAddressComplement=casa&billingAddressNeighborhood=Centro&billingAddressCity=PortoAlegre&billingAddressState=RS&billingAddressPostcode=95700000&notificationUrl=http://teste.com`

**Resposta**

.. code:: javascript

   {
      "success":true,
      "data":{
         "charges":[
            {
               "code":"18340000002",
               "amount":"150.00",
               "dueDate":"10/10/2018",
               "link":"https://sandboxgateway.branvo.com.br/api/boleto/v1/printall/38a593dc6206decd08003f7a639e8f99",
               "installmentLink":"https://sandboxgateway.branvo.com.br/api/boleto/v1/print/3604cb1efc7da0cd0d3bf3dd81f83709"
            }
         ]
      }
   }

*****************
Emissão com split
*****************

:guilabel:`GET` ``https://gateway.branvo.com/api/boleto/v1/emission/``

**O que é split de pagamento?**

É um sistema que realiza as divisões de pagamentos aos seus fornecedores, realizando uma única cobrança do seu
cliente e simplificando o gerenciamento de recebíveis, dividindo o valor entre uma conta principal e outras contas secundárias.

==========
Requisição
==========

.. table::
   :widths: auto

   +----------------------------+-----------------+------------------------------------------------------------+
   | Parâmetro                  | Tipo            | Descrição                                                  |
   +============================+=================+============================================================+
   | split                      | int             | | 0 (false) ou 1 (true)                                    |
   |                            |                 | | Transação será feita por split de pagamentos             |
   |                            |                 | | Se for 1, todos os parâmetros abaixo tornam-se           |
   |                            |                 | | obrigatórios                                             |
   +----------------------------+-----------------+------------------------------------------------------------+
   | splitType                  | string          | | Os valores enviados serão em formato valor ou            |
   |                            |                 | | porcentagem                                              |
   |                            |                 | | Valores aceitos: [val (valor), perc (porcentagem)]       |
   +----------------------------+-----------------+------------------------------------------------------------+
   | splitToken                 | array           | | Array com valores a serem repartidos                     |
   |                            |                 | | Ex.: splitAmount[0] = 100.00, splitAmount[1] = 50.00...  |
   +----------------------------+-----------------+------------------------------------------------------------+
   | splitAmount                | array           | | Array com tokens das contas que irão receber os valores  |
   |                            |                 | | **OBS.:** Respeitar a ordem do parâmetro splitAmount,    |
   |                            |                 | | Ex.: Se splitAmount[0] = 100.00 e splitToken[0] = 123    |
   |                            |                 | | a conta referente ao token 123 receberá R$ 100,00,       |
   |                            |                 | | e assim por diante                                       |
   +----------------------------+-----------------+------------------------------------------------------------+

========
Resposta
========

**Sucesso**

.. code:: javascript

   {
      "success": bool,
      "data":{
         "charges":[
            {
               "code": string,
               "amount": decimal,
               "dueDate": string,
               "link": string,
               "installmentLink": string
            }
         ]
      }
   }

**Falha**

.. code:: javascript

   {
      "success": false,
      "data": {
         "message": "{mensagem de erro correspondente}"
      }
   }

=======
Exemplo
=======

**Requisição**

.. parsed-literal::

    `https://sandboxgateway.branvo.com.br/api/boleto/v1/emission/?token=D5DCEHY67T159B3282A1950EF221535F&description=Boletoteste&reference=1876000031&amount=150.00&dueDate=10/10/2018&installments=1&maxOverdueDays=0&fine=0.00&interest=0.00&discountAmount=0.00&discountDays=0.00&payerName=Testepagador&payerCpf=48824235700&payerEmail=tecnico@branvo.com&payerSecondaryEmail=tecnico@branvo.com&payerPhone=5499999999&payerBirthDate=01/04/1990&billingAddressStreet=Ruateste&billingAddressNumber=100&billingAddressComplement=casa&billingAddressNeighborhood=Centro&billingAddressCity=PortoAlegre&billingAddressState=RS&billingAddressPostcode=95700000&notificationUrl=http://teste.com`

**Resposta**

.. code:: javascript

   {
      "success":true,
      "data":{
         "charges":[
            {
               "code":"18340000002",
               "amount":"150.00",
               "dueDate":"10/10/2018",
               "link":"https://sandboxgateway.branvo.com.br/api/boleto/v1/printall/38a593dc6206decd08003f7a639e8f99",
               "installmentLink":"https://sandboxgateway.branvo.com.br/api/boleto/v1/print/3604cb1efc7da0cd0d3bf3dd81f83709"
            }
         ]
      }
   }

************
Cancelamento
************

:guilabel:`GET` ``https://gateway.branvo.com/api/boleto/v1/cancel/``

**Realiza a solicitação de cancelamento do boleto, pelo seu número e vencimento**

.. attention::

   Esta operação **NÃO** garante a baixa do boleto, ela apenas é
   confirmada mediante aprovação e retorno bancário. Ou seja, será solicitada
   a baixa do boleto, o banco irá analisar, processar a baixa e retornar, e através
   da sua URL de notificação ele deverá ser efetivamente cancelado pela
   aplicação usuário BranvoPay

==========
Requisição
==========

.. table::
   :widths: auto

   +----------------------------+-----------------+------------------------------------------------------------+
   | Parâmetro                  | Tipo            | Descrição                                                  |
   +============================+=================+============================================================+
   | token*                     | string          | Token da conta referente à transação                       |
   +----------------------------+-----------------+------------------------------------------------------------+
   | code*                      | string          | | “Nosso Número“ do boleto                                 |
   |                            |                 | | **OBS.:** Deve ser o número gerado pelo banco, e não o   |
   |                            |                 | | número do pedido                                         |
   +----------------------------+-----------------+------------------------------------------------------------+
   | vencimento*                | string          | Descrição do boleto                                        |
   +----------------------------+-----------------+------------------------------------------------------------+

.. note::
   Parâmetros com asterisco ( * ) são obrigatórios

========
Resposta
========

**Sucesso**

.. code:: javascript

   {
      "success": bool,
      "data": {
      "charges": [
         {
            "code": string,
            "amount": decimal,
            "dueDate": string,
            "link": string,
            "installmentLink": string
         }
      ]
   }

**Falha**

.. code:: javascript

   {
      "success": false,
      "data": {
         "message": "{mensagem de erro correspondente}"
      }
   }

=======
Exemplo
=======

**Requisição**

.. parsed-literal::

    `https://sandboxgateway.branvo.com.br/api/boleto/v1/cancel/?token=D5DCEHY67T159B3282A1950EF221535F&code=18340000002&vencimento=10/10/2018`

**Resposta**

.. code:: javascript

   {
      "success":true,
      "data":{
      "charges":[
         {
            "code":"18340000002",
            "amount":"150.00",
            "dueDate":"10/10/2018",
            "cancel":true
         }
      ]
   }

******
Testes
******

========
Usuários
========

Para realizar os testes em ambiente sandbox, pode ser usado o seguinte usuário (token) de testes:

.. table::
   :widths: auto

   +----------------------------+----------------------------------+
   | Usuário                    | Token                            |
   +============================+==================================+
   | Test Main Account          | 77F8ECCA8E9CA66FF5DB48812CD07544 |
   +----------------------------+----------------------------------+

Para realização de testes de split de pagamentos, pode ser utilizado, juntamente com o token de testes acima, o seguinte token:

.. table::
   :widths: auto

   +----------------------------+----------------------------------+
   | Usuário                    | Token                            |
   +============================+==================================+
   | Test Split  Account        | 321D27C059A410EF0BD2C21A136CB29A |
   +----------------------------+----------------------------------+