##########
Endereço
##########

.. important::
  Por questões internas, em ambiente sandbox, os cadastros de endereços de clientes não serão salvos permanentemente, sendo resetados de 30 em 30 minutos.

********************
Adicionar Endereço
********************

Referência
============

:guilabel:`POST` ``https://api.ebranvo.com/v1/clientes/enderecos``

.. include:: ./examples/table/address.rst

.. note::
   Parâmetros com asterisco ( * ) são obrigatórios


Exemplo de Requisição
=======================

.. literalinclude:: ./examples/request/request.json
   :language: JSON

Resposta
=========

Status 201: CREATED
--------------------

.. literalinclude:: examples/response/success.json
  :language: JSON

Status 400: Bad Request
-------------------------

Ocorre quando há algum problema nos parâmetros enviados.

.. literalinclude:: examples/response/fail.json
  :language: JSON



********************
Atualizar Endereço
********************

Referência
============

:guilabel:`PUT` ``https://api.ebranvo.com/v1/clientes/enderecos``

.. include:: ./examples/table/address-upd.rst

.. note::
   Parâmetros com asterisco ( * ) são obrigatórios


Exemplo de Requisição
=======================

.. literalinclude:: ./examples/request/request-upd.json
   :language: JSON

Resposta
=========

Status 200: OK
--------------------

.. literalinclude:: examples/response/success.json
  :language: JSON

Status 400: Bad Request
-------------------------

Ocorre quando há algum problema nos parâmetros enviados.

.. literalinclude:: examples/response/fail.json
  :language: JSON



**********************
Consultar Endereços
**********************

Referência
===========

:guilabel:`GET` ``https://api.ebranvo.com/v1/clientes/{id}/enderecos``

Basta uma requisição GET simples para esse endpoint, informando o ID do cliente na URL para retornar os endereços do mesmo.

Resposta
=========

Status 200: OK
--------------------

.. literalinclude:: examples/response/success-all.json
  :language: JSON

Status 400: Bad Request
-------------------------

Ocorre quando há algum problema nos parâmetros enviados.

.. literalinclude:: examples/response/fail.json
  :language: JSON



****************************
Consultar Endereço por ID
****************************

Referência
===========

:guilabel:`GET` ``https://api.ebranvo.com/v1/clientes/enderecos/{id}``

Basta uma requisição GET simples para esse endpoint, informando o ID do endereço na URL para retornar o endereço em questão.

Resposta
=========

Status 200: OK
--------------------

.. literalinclude:: examples/response/success.json
  :language: JSON

Status 400: Bad Request
-------------------------

Ocorre quando há algum problema nos parâmetros enviados.

.. literalinclude:: examples/response/fail.json
  :language: JSON




***********************
Excluir Endereço
***********************

Referência
===========

:guilabel:`DELETE` ``https://api.ebranvo.com/v1/clientes/enderecos/{id}``

Basta uma requisição DELETE simples para esse endpoint, informando o ID do endereço na URL para excluir o endereço em questão.

Resposta
=========

Status 200: OK
--------------------

.. literalinclude:: examples/response/success.json
  :language: JSON

Status 400: Bad Request
-------------------------

Ocorre quando há algum problema nos parâmetros enviados.

.. literalinclude:: examples/response/fail.json
  :language: JSON