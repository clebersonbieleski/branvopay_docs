##########
Cliente
##########

.. important::
  Por questões internas, em ambiente sandbox, os cadastros de clientes não serão salvos permanentemente, sendo resetados de 30 em 30 minutos.

******************
Adicionar Cliente
******************

Referência
============

:guilabel:`POST` ``https://api.ebranvo.com/v1/clientes``

.. include:: ./examples/table/customer.rst

.. note::
   Parâmetros com asterisco ( * ) são obrigatórios


Exemplo de Requisição
=======================

.. literalinclude:: ./examples/request/request.json
   :language: JSON

Resposta
=========

Status 201: CREATED
--------------------

.. literalinclude:: examples/response/success.json
  :language: JSON

Status 400: Bad Request
-------------------------

Ocorre quando há algum problema nos parâmetros enviados.

.. literalinclude:: examples/response/fail.json
  :language: JSON



********************
Atualizar Cliente
********************

Referência
============

:guilabel:`PUT` ``https://api.ebranvo.com/v1/clientes``

.. include:: ./examples/table/customer-upd.rst

.. note::
   Parâmetros com asterisco ( * ) são obrigatórios


Exemplo de Requisição
=======================

.. literalinclude:: ./examples/request/request-upd.json
   :language: JSON

Resposta
=========

Status 200: OK
--------------------

.. literalinclude:: examples/response/success.json
  :language: JSON

Status 400: Bad Request
-------------------------

Ocorre quando há algum problema nos parâmetros enviados.

.. literalinclude:: examples/response/fail.json
  :language: JSON



**********************
Consultar Clientes
**********************

Referência
===========

:guilabel:`GET` ``https://api.ebranvo.com/v1/clientes/pagina/{numero-pagina}``

A API de clientes realiza a consulta com paginação, onde a variável {numero-pagina} corresponde ao número da página desejado (1, 2, 3, ...) 
e são retornados 50 clientes por página.

Resposta
=========

Status 200: OK
--------------------

.. literalinclude:: examples/response/success-all.json
  :language: JSON

Status 400: Bad Request
-------------------------

Ocorre quando há algum problema nos parâmetros enviados.

.. literalinclude:: examples/response/fail.json
  :language: JSON



****************************
Consultar Cliente por ID
****************************

Referência
===========

:guilabel:`GET` ``https://api.ebranvo.com/v1/clientes/{id}``

Basta uma requisição GET simples para esse endpoint, informando o ID do cliente na URL para retornar o cliente em questão.

Resposta
=========

Status 200: OK
--------------------

.. literalinclude:: examples/response/success.json
  :language: JSON

Status 400: Bad Request
-------------------------

Ocorre quando há algum problema nos parâmetros enviados.

.. literalinclude:: examples/response/fail.json
  :language: JSON




***********************
Excluir Cliente
***********************

Referência
===========

:guilabel:`DELETE` ``https://api.ebranvo.com/v1/clientes/{id}``

Basta uma requisição DELETE simples para esse endpoint, informando o ID do cliente na URL para excluir o cliente em questão.

Resposta
=========

Status 200: OK
--------------------

.. literalinclude:: examples/response/success.json
  :language: JSON

Status 400: Bad Request
-------------------------

Ocorre quando há algum problema nos parâmetros enviados.

.. literalinclude:: examples/response/fail.json
  :language: JSON