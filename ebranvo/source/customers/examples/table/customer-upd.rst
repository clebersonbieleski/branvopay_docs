.. table::
   :widths: auto

   +--------------------------+-----------------+-------------------------------------------------------------------+
   | Parâmetros               | Tipo            | Descrição                                                         |
   +==========================+=================+===================================================================+
   | id*                      | id              | ID do cliente na e-Branvo                                         |
   +--------------------------+-----------------+-------------------------------------------------------------------+
   | Tipo                     | string          | Tipo do cliente (PF/PJ)                                           |
   +--------------------------+-----------------+-------------------------------------------------------------------+
   | name                     | string          | Nome/Razão social do cliente                                      |
   +--------------------------+-----------------+-------------------------------------------------------------------+
   | tradingName              | string          | Nome fantasia do cliente                                          |
   +--------------------------+-----------------+-------------------------------------------------------------------+
   | stateRegister            | string          | Inscrição estadual                                                |
   +--------------------------+-----------------+-------------------------------------------------------------------+
   | document                 | string          | Documento (CPF/CNPJ) do cliente                                   |
   +--------------------------+-----------------+-------------------------------------------------------------------+
   | phone                    | string          | Telefone do cliente com DDD                                       |
   +--------------------------+-----------------+-------------------------------------------------------------------+
   | birthDate                | string          | Data de nascimento no formato AAAA-MM-DD                          |
   +--------------------------+-----------------+-------------------------------------------------------------------+
   | mail                     | decimal         | E-mail do cliente                                                 |
   +--------------------------+-----------------+-------------------------------------------------------------------+
   | password                 | string          | Senha do cliente                                                  |
   +--------------------------+-----------------+-------------------------------------------------------------------+
   | gender                   | int             | Gênero do cliente? (1 - feminino / 2 - masculino)                 |
   +--------------------------+-----------------+-------------------------------------------------------------------+
   | active                   | boolean         | Cliente ativo?                                                    |
   +--------------------------+-----------------+-------------------------------------------------------------------+
   | addresses                | array           | Endereços do cliente. Objetos do tipo endereço_                   |
   +--------------------------+-----------------+-------------------------------------------------------------------+

.. _endereço: ./addresses