.. e-Branvo API documentation master file, created by
   sphinx-quickstart on Mon Nov  5 13:20:02 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. toctree::
   :hidden:
   :maxdepth: 5
   :caption: Produtos:
   :glob:

   products/index
   products/variations/index
   products/brands/index
   products/categories/index
   products/images/index
   products/variationTypes/index
   products/variationOptions/index

.. toctree::
   :hidden:
   :maxdepth: 5
   :caption: Clientes:
   :glob:

   customers/index
   customers/addresses/index

.. toctree::
   :hidden:
   :maxdepth: 5
   :caption: Pedidos:
   :glob:

   orders/index
   orders/items/index
   orders/status

.. toctree::
   :hidden:
   :maxdepth: 5
   :caption: Testes:
   :glob:

   tests/index

##########
Introdução
##########

******
Resumo
******

Esta documentação tem o objetivo de auxiliar o desenvolvedor na integração com a API da plataforma e-Branvo Lojas Virtuais.
Estarão descritos a seguir todos os mecanismos necessários para a implementação da integração.

A API da e-Branvo Lojas Virtuais foi escrita seguindo os padrões da tecnologia REST (Representational State Transfer), que
é o padrão atual do mercado e da maioria das APIs da web. Dessa maneira,
possibilitamos aos desenvolvedores uma integração que independe da linguagem de programação escolhida.

************
Metodologias
************

A integração pode ser realizada em dois endpoints: um para ambiente sandbox e um para ambiente de produção. Recomendamos iniciar a integração
em ambiente sandbox até a finalização de todos os testes necessários. Os endpoits da API BranvoPay são os seguintes:

**Sandbox**

``https://sandbox-api.ebranvo.com/``

**Produção**

``https://api.ebranvo.com/``

.. important::
   Para envio de requisições para o ambiente sandbox da API <https://sandbox-api.ebranvo.com>, deve ser utilizado o token de testes. O token de produção será inválido neste ambiente, sendo válido apenas no ambiente de produção da API. Para obtenção do token de testes, visite a seção testes_.


As requisições da API serão realizadas através dos métodos HTTP. Cada um dos métodos possuirá uma responsabilidade
distinta e seguirá um padrão de requisição. Os métodos utilizados
na API da e-Branvo são os seguintes:

=================== ======================= ======================================================
Método              Utilização                                Exemplo
=================== ======================= ======================================================
:guilabel:`POST`    Criação de recursos     Adicionar um produto, adicionar uma categoria
:guilabel:`GET`     Consulta de recursos    Consultar dados de um produto
:guilabel:`PUT`     Atualização de recursos Atualizar dados de uma categoria
:guilabel:`DELETE`  Exclusão de recursos    Excluir uma categoria
=================== ======================= ======================================================

Os retornos da API sempre serão acompanhados dos códigos HTTP correspondentes ao resultado da requisição. O formato do corpo do retorno
é JSON. Os códigos HTTP utilizados pela API BranvoPay são os seguintes:

========================== ============================================================
Código                     Descrição
========================== ============================================================
*200 - OK*                  Quando uma requisição obtém o retorno com sucesso
*201 - CREATED*             .. line-block::
                             Quando uma requisição cria um recurso com sucesso
                             Ex.: Criação de produto
*400 - BAD REQUEST*         .. line-block::
                             Quando uma requisição possui algum problema em seus dados
                             Ex.: Nome de produto inválido, CPF do cliente inválido
*401 - UNAUTHORIZED*        Problemas de autenticação
*403 - FORBIDDEN*           Problemas de autorização
*404 - NOT FOUND*           URL da requisição não for encontrada
*405 - METHOD NOT ALLOWED*  .. line-block::
                             A URL não pode ser acessada através do método solicitado
                             Ex.: Recurso acessível via GET tenta ser acessado via PUT
========================== ============================================================

************
Autenticação
************

A autenticação na API da e-Branvo é realizada através do token da conta. Em todas as requisições,
o token deve ser enviado através do campo 'Account-Token', no header de cada operação.

.. code:: javascript

    Account-Token: seu_token_aqui

*****************
Envio de Dados
*****************

A API da e-Branvo aceita envio de dados no tipo `application/json`, com exceção dos produtos e fotos,
que aceitam também `multipart/form-data` para envio de arquivos binários. Essa informação é passada no `header` de cada requisição, juntamente com o `Account-Token`:

.. code:: javascript

    Content-Type: application/json

ou

.. code:: javascript

    Content-Type: multipart/form-data


******
Testes
******

Para a realização de testes na API e-Branvo, recomendamos a utilização da ferramenta POSTMAN, disponível gratuitamente no link:
<https://www.getpostman.com/>. Para mais detalhes sobre a operação de testes na API, visite a seção de testes_.

.. _testes: ./tests

.. note::
   A API da e-Branvo trabalha com um sistema de cache para requisições em curto intervalo de tempo. Recomendamos intervalo de 1 segundo entre requisições do mesmo tipo.