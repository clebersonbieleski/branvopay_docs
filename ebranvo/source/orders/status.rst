##############################
Tabela de Status de Pedidos
##############################

Os status de pedidos sempre são no formato de números inteiros, cujos valores correspondem às descrições conforme tabela abaixo:

.. include:: ./examples/table/status.rst