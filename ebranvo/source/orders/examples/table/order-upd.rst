.. table::
   :widths: auto

   +--------------------------+-----------------+-------------------------------------------------------------------+
   | Parâmetros               | Tipo            | Descrição                                                         |
   +==========================+=================+===================================================================+
   | id*                      | int             | Número do pedido                                                  |
   +--------------------------+-----------------+-------------------------------------------------------------------+
   | status*                  | int             | Status do pedido. Consultar tabela de status_                     |
   +--------------------------+-----------------+-------------------------------------------------------------------+

   .. _status: ./status.html
