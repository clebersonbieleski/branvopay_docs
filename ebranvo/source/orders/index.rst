##########
Pedido
##########

******************
Adicionar Pedido
******************

Referência
============

:guilabel:`POST` ``https://api.ebranvo.com/v1/pedidos``

.. include:: ./examples/table/order.rst

.. note::
   Parâmetros com asterisco ( * ) são obrigatórios


Exemplo de Requisição
=======================

.. literalinclude:: ./examples/request/request.json
   :language: JSON

Resposta
=========

Status 201: CREATED
--------------------

.. literalinclude:: examples/response/success.json
  :language: JSON

Status 400: Bad Request
-------------------------

Ocorre quando há algum problema nos parâmetros enviados.

.. literalinclude:: examples/response/fail.json
  :language: JSON


****************************
Atualizar Status do Pedido
****************************

Referência
============

:guilabel:`PUT` ``https://api.ebranvo.com/v1/pedidos``

.. include:: ./examples/table/order-upd.rst

.. note::
   Parâmetros com asterisco ( * ) são obrigatórios


Exemplo de Requisição
=======================

.. literalinclude:: ./examples/request/request-upd.json
   :language: JSON

Resposta
=========

Status 200: OK
--------------------

.. literalinclude:: examples/response/success-upd.json
  :language: JSON

Status 400: Bad Request
-------------------------

Ocorre quando há algum problema nos parâmetros enviados.

.. literalinclude:: examples/response/fail.json
  :language: JSON

****************
Consultar Pedido
****************

Referência
============

:guilabel:`GET` ``https://api.ebranvo.com/v1/pedidos/{id}``

Basta uma requisição GET simples para esse endpoint,
informando o ID do produto na URL para retornar o pedido em questão.

Resposta
=========

Status 200: OK
--------------------

.. literalinclude:: examples/response/success.json
  :language: JSON

*****************
Consultar Pedidos
*****************

Referência
============

:guilabel:`GET` ``https://api.ebranvo.com/v1/pedidos/pagina/{numero-pagina}``

A API de pedidos realiza a consulta com paginação, onde a variável {numero-pagina}
corresponde ao número da página desejada (1, 2, 3, …) e são retornados 50 pedidos por página.
Os pedidos são retornados em ordem DECRESCENTE.

De forma opcional, pode ser passado via `query param` um parâmetro `date`, conforme especificação abaixo:

+--------------------------+-----------------+-------------------------------------------------------------------+
| Parâmetro                | Tipo            | Descrição                                                         |
+==========================+=================+===================================================================+
| date                     | string          | | Caso passado este parâmetro, aparecerão os pedidos que foram    |
|                          |                 | | atualizados a partir desta data e hora.                         |
|                          |                 | | Formato: AAAA-MM-DD HH:MM:SS                                    |
|                          |                 | | Ex: ``{url}/v1/pedidos/pagina/1?date=2019-01-01 10:00:00``      |
+--------------------------+-----------------+-------------------------------------------------------------------+

Resposta
=========

Status 200: OK
--------------------

.. literalinclude:: examples/response/success-all.json
  :language: JSON

**************
Remover Pedido
**************

Referência
============

:guilabel:`DELETE` ``https://api.ebranvo.com/v1/pedidos/{id}``

Basta uma requisição DELETE simples para esse endpoint,
informando o ID na URL para remover o pedido em questão.

Resposta
=========

Status 200: OK
--------------------

.. literalinclude:: examples/response/success.json
  :language: JSON

******************************
Inserir Nota Fiscal do Pedido
******************************

Referência
============

:guilabel:`POST` ``https://api.ebranvo.com/v1/pedidos/{id}/nf``

Informe o ID do pedido, ao qual pertence a nota fiscal, na URL.

.. include:: ./examples/table/order-nf.rst

.. note::
   Parâmetros com asterisco ( * ) são obrigatórios


Exemplo de Requisição
=======================

.. literalinclude:: ./examples/request/request-nf.json
   :language: JSON

Resposta
=========

Status 200: OK
--------------------

.. literalinclude:: examples/response/success-nf.json
  :language: JSON

Status 400: Bad Request
-------------------------

Ocorre quando há algum problema nos parâmetros enviados.

.. literalinclude:: examples/response/fail.json
  :language: JSON

*******************************
Atualizar Nota Fiscal do Pedido
*******************************

Referência
============

:guilabel:`PUT` ``https://api.ebranvo.com/v1/pedidos/{id}/nf``

Informe o ID do pedido, ao qual pertence a nota fiscal, na URL.

.. include:: ./examples/table/order-nf.rst

.. note::
   Parâmetros com asterisco ( * ) são obrigatórios


Exemplo de Requisição
=======================

.. literalinclude:: ./examples/request/request-nf.json
   :language: JSON

Resposta
=========

Status 200: OK
--------------------

.. literalinclude:: examples/response/success-nf.json
  :language: JSON

Status 400: Bad Request
-------------------------

Ocorre quando há algum problema nos parâmetros enviados.

.. literalinclude:: examples/response/fail.json
  :language: JSON


********************************
Consultar Nota Fiscal do Pedido
********************************

Referência
============

:guilabel:`GET` ``https://api.ebranvo.com/v1/pedidos/{id}/nf``

Basta uma requisição GET simples para esse endpoint, informando o ID do pedido na URL, para retornar a nota fiscal do pedido em questão.

Resposta
=========

Status 200: OK
--------------------

.. literalinclude:: examples/response/success-all-nf.json
  :language: JSON

Status 400: Bad Request
-------------------------

Ocorre quando há algum problema nos parâmetros enviados.

.. literalinclude:: examples/response/fail.json
  :language: JSON

*****************************
Remover Nota Fiscal do Pedido
*****************************

Referência
============

:guilabel:`DELETE` ``https://api.ebranvo.com/v1/pedidos/{id}/nf``

Basta uma requisição DELETE simples para esse endpoint,
informando o ID do pedido na URL, para remover a nota fiscal do pedido em questão.

Resposta
=========

Status 200: OK
--------------------

.. literalinclude:: examples/response/success-nf.json
  :language: JSON

Status 400: Bad Request
-------------------------

Ocorre quando há algum problema nos parâmetros enviados.

.. literalinclude:: examples/response/fail.json
  :language: JSON
