###############
Item de pedido
###############

Não há endpoints para os items, eles só podem ser inseridos juntamente com o pedido.

******************
Referência
******************

.. include:: ./examples/table/item.rst

¹Caso o parâmetro idProduct seja informado,	o parâmetro idProductVariation deixa de ser obrigatório e vice-versa.

.. note::
   Parâmetros com asterisco ( * ) são obrigatórios


Exemplo
=======================

.. literalinclude:: ./examples/request/request.json
   :language: JSON