#################
Testando a API
#################

***************************
Operação dos Testes na API
***************************

Para utilizar a API da e-Branvo em ambiente sandbox, você precisa utilizar o seu token de testes. Para obtê-lo, envie uma solicitação do token de testes para o e-mail <suporte@branvo.com>.

.. important::
   Para envio de requisições para o ambiente sandbox da API <https://sandbox-api.ebranvo.com>, deve ser utilizado somente o token de testes. O token de produção será inválido neste ambiente, sendo válido apenas no ambiente de produção da API.

****************************************
Simulando envio de arquivos no POSTMAN
****************************************

Através da ferramenta POSTMAN, é possível realizar o envio de arquivos via POST, como pode ser feito nas APIs de produtos e de fotos da e-Branvo.

Para isso, a URL e método HTTP devem estar no seguinte formato:

.. image:: url.png

Depois, o Body da requisição deve estar no formato `form-data`, e o campo desejado deve ser alterado para tipo File:

.. image:: body.png

Feita essa configuração, a requisição já pode ser enviada.

Caso tenha dúvidas em como traduzir isso para o seu código de integração, o POSTMAN fornece o código da requisição feita, basta clicar no link Code no canto superior direito da tela:

.. image:: code.png

E, no modal que irá abrir, selecionar a tecnologia de sua preferência no campo de seleção no canto superior esquerdo:

.. image:: lang.png