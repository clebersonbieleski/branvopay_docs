.. Branvo Store API documentation master file, created by
   sphinx-quickstart on Mon Nov  5 13:20:02 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

###################
Marca
###################

*******************
Adicionar Marca
*******************

Referência
===========

:guilabel:`POST` ``https://api.ebranvo.com/v1/produtos/marcas``

.. include:: ./examples/table/brand.rst

.. note::
   Parâmetros com asterisco ( * ) são obrigatórios


Exemplo de Requisição
======================

.. literalinclude:: ./examples/request/request.json
  :language: JSON

Resposta
=========

Status 201: CREATED
--------------------

.. literalinclude:: examples/response/success.json
  :language: JSON

Status 400: Bad Request
-------------------------

Ocorre quando há algum problema nos parâmetros enviados.

.. literalinclude:: examples/response/fail.json
  :language: JSON



*******************
Atualizar Marca
*******************

Referência
===========

:guilabel:`PUT` ``https://api.ebranvo.com/v1/produtos/marcas``

.. include:: ./examples/table/brand-upd.rst

.. note::
   Parâmetros com asterisco ( * ) são obrigatórios


Exemplo de Requisição
======================

.. literalinclude:: ./examples/request/request-upd.json
  :language: JSON

Resposta
=========

Status 200: OK
--------------------

.. literalinclude:: examples/response/success.json
  :language: JSON

Status 400: Bad Request
-------------------------

Ocorre quando há algum problema nos parâmetros enviados.

.. literalinclude:: examples/response/fail.json
  :language: JSON



*******************
Consultar Marcas
*******************

Referência
===========

:guilabel:`GET` ``https://api.ebranvo.com/v1/produtos/marcas``

Basta uma requisição GET simples para esse endpoint para retornar todas as marcas cadastradas em uma loja.

Resposta
=========

Status 200: OK
--------------------

.. literalinclude:: examples/response/success-all.json
  :language: JSON

Status 400: Bad Request
-------------------------

Ocorre quando há algum problema nos parâmetros enviados.

.. literalinclude:: examples/response/fail.json
  :language: JSON



***********************
Consultar Marca por ID
***********************

Referência
===========

:guilabel:`GET` ``https://api.ebranvo.com/v1/produtos/marcas/{id}``

Basta uma requisição GET simples para esse endpoint, informando o ID da marca na URL para retornar a marca em questão.

Resposta
=========

Status 200: OK
--------------------

.. literalinclude:: examples/response/success.json
  :language: JSON

Status 400: Bad Request
-------------------------

Ocorre quando há algum problema nos parâmetros enviados.

.. literalinclude:: examples/response/fail.json
  :language: JSON




***********************
Excluir Marca
***********************

Referência
===========

:guilabel:`DELETE` ``https://api.ebranvo.com/v1/produtos/marcas/{id}``

Basta uma requisição DELETE simples para esse endpoint, informando o ID da marca na URL para excluir a marca em questão.

Resposta
=========

Status 200: OK
--------------------

.. literalinclude:: examples/response/success.json
  :language: JSON

Status 400: Bad Request
-------------------------

Ocorre quando há algum problema nos parâmetros enviados.

.. literalinclude:: examples/response/fail.json
  :language: JSON