###################
Produto
###################

*******************
Adicionar Produto
*******************

Referência
===========

:guilabel:`POST` ``https://api.ebranvo.com/v1/produtos``

.. include:: ./examples/table/product.rst

.. note::
   Parâmetros com asterisco ( * ) são obrigatórios

As requisições para produtos podem ser feitas tanto utilizando `application/json` quanto `multipart/form-data`.
No caso de envio de imagens por arquivo (`uploaded file`), o envio deve obrigatóriamente 
ser por `multipart/form-data`, caso contrário, as imagens serão desconsideradas.

Explicando a funcionalidade das categorias, imagens, marca e variações:
    | Para inserção de categorias, são aceitos arrays de objetos e arrays de IDs de itens já existentes.
    | No caso de array de objetos, eles irão seguir as regras descritas na sessão do respectivo objeto.
    | No caso de array de IDs, os IDs serão validados e relacionados com o produto em questão.
    | Para inserção de marcas, é aceito um objeto do tipo marca ou ID de uma marca já existente.
    | Para inserção de imagens, são aceitos objetos do tipo imagem ou IDs de imagens já existentes.
    | Para inserção de variações, são aceitos apenas objetos do tipo variação.


Exemplos de Requisição
======================

JSON
-----

.. literalinclude:: ./examples/request/request.json
  :language: JSON

FORM-DATA
----------

.. include:: ./examples/request/request.rst

Resposta
=========

Status 201: CREATED
--------------------

.. literalinclude:: examples/response/success.json
  :language: JSON

Status 400: Bad Request
-------------------------

Ocorre quando há algum problema nos parâmetros enviados.

.. literalinclude:: examples/response/fail.json
  :language: JSON



*******************
Atualizar Produto
*******************

Referência
===========

:guilabel:`POST` ``https://api.ebranvo.com/v1/produtos/atualizar``

.. note::
   A atualização de produtos, diferente de outros itens, deve ser feita usando o método :guilabel:`POST`, pois o método :guilabel:`PUT` não aceita `multipart/form-data` para o caso de envio de arquivos de imagens.

.. include:: ./examples/table/product-upd.rst

.. note::
   Parâmetros com asterisco ( * ) são obrigatórios

Explicando a funcionalidade das categorias, imagens e marca:
    | Para atualização de produtos, são aceitos arrays de objetos e arrays de IDs de itens já existentes.
    | No caso de array de objetos, eles irão seguir as regras descritas na sessão do respectivo objeto.
    | No caso de array de IDs, os IDs serão validados e relacionados com o produto em questão.
    | Caso informe no array IDs que ainda não estão relacionados ao produto, eles serão validados e relacionados.
    | Caso informe um array sem IDs que já estão relacionados ao produto, eles serão removidos.

Exemplo prático usando categorias:
    | Inseri um produto com as categorias [1, 2, 3].
    | Fiz uma requisição de atualização, enviando no array `categories` os valores [1, 2].
    | Nesse caso, a categoria 3 será removida.
    |
    | Inseri um produto com as categorias [1, 2, 3].
    | Fiz uma requisição de atualização, enviando no array `categories` os valores [1, 2, 3, 4].
    | Nesse caso, as categorias [1, 2, 3] serão mantidas, e a categoria 4 será adicionada ao produto.

Explicando a funcionalidade das variações:
    | Para atualização de produtos, são aceitos arrays de objetos de variações. 
    | Se for informado o ID da variação no objeto, a variação será atualizada.
    | Caso contrário, as variações informadas serão adicionadas ao produto.


Exemplos de Requisição
=======================

JSON
-----

.. literalinclude:: ./examples/request/request-upd.json
  :language: JSON

FORM-DATA
----------

.. include:: ./examples/request/request-upd.rst

Resposta
=========

Status 200: OK
--------------------

.. literalinclude:: examples/response/success-upd.json
  :language: JSON

Status 400: Bad Request
-------------------------

Ocorre quando há algum problema nos parâmetros enviados.

.. literalinclude:: examples/response/fail.json
  :language: JSON



**********************
Consultar Produtos
**********************

Referência
===========

:guilabel:`GET` ``https://api.ebranvo.com/v1/produtos/pagina/{numero-pagina}``

A API de produtos realiza a consulta com paginação, onde a variável {numero-pagina} corresponde ao número da página desejado (1, 2, 3, ...) 
e são retornados 50 produtos por página.

Resposta
=========

Status 200: OK
--------------------

.. literalinclude:: examples/response/success-all.json
  :language: JSON

Status 400: Bad Request
-------------------------

Ocorre quando há algum problema nos parâmetros enviados.

.. literalinclude:: examples/response/fail.json
  :language: JSON



****************************
Consultar Produto por ID
****************************

Referência
===========

:guilabel:`GET` ``https://api.ebranvo.com/v1/produtos/{id}``

Basta uma requisição GET simples para esse endpoint, informando o ID do produto na URL para retornar o produto em questão.

Resposta
=========

Status 200: OK
--------------------

.. literalinclude:: examples/response/success.json
  :language: JSON

Status 400: Bad Request
-------------------------

Ocorre quando há algum problema nos parâmetros enviados.

.. literalinclude:: examples/response/fail.json
  :language: JSON




***********************
Excluir Produto
***********************

Referência
===========

:guilabel:`DELETE` ``https://api.ebranvo.com/v1/produtos/{id}``

Basta uma requisição DELETE simples para esse endpoint, informando o ID do produto na URL para excluir o produto em questão.

Resposta
=========

Status 200: OK
--------------------

.. literalinclude:: examples/response/success.json
  :language: JSON

Status 400: Bad Request
-------------------------

Ocorre quando há algum problema nos parâmetros enviados.

.. literalinclude:: examples/response/fail.json
  :language: JSON