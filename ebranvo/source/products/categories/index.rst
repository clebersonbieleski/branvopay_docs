.. e-Branvo API documentation master file, created by
   sphinx-quickstart on Mon Nov  5 13:20:02 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

###################
Categoria
###################

********************
Adicionar Categoria
********************

Referência
===========

:guilabel:`POST` ``https://api.ebranvo.com/v1/produtos/categorias``

.. include:: ./examples/table/category.rst

.. note::
   Parâmetros com asterisco ( * ) são obrigatórios


Exemplo de Requisição
======================

.. literalinclude:: ./examples/request/request.json
  :language: JSON

Resposta
=========

Status 201: CREATED
--------------------

.. literalinclude:: examples/response/success.json
  :language: JSON

Status 400: Bad Request
-------------------------

Ocorre quando há algum problema nos parâmetros enviados.

.. literalinclude:: examples/response/fail.json
  :language: JSON



*******************
Atualizar Categoria
*******************

Referência
===========

:guilabel:`PUT` ``https://api.ebranvo.com/v1/produtos/categorias``

.. include:: ./examples/table/category-upd.rst

.. note::
   Parâmetros com asterisco ( * ) são obrigatórios


Exemplo de Requisição
======================

.. literalinclude:: ./examples/request/request-upd.json
  :language: JSON

Resposta
=========

Status 200: OK
--------------------

.. literalinclude:: examples/response/success.json
  :language: JSON

Status 400: Bad Request
-------------------------

Ocorre quando há algum problema nos parâmetros enviados.

.. literalinclude:: examples/response/fail.json
  :language: JSON



**********************
Consultar Categorias
**********************

Referência
===========

:guilabel:`GET` ``https://api.ebranvo.com/v1/produtos/categorias``

Basta uma requisição GET simples para esse endpoint para retornar todas as categorias cadastradas em uma loja.

Resposta
=========

Status 200: OK
--------------------

.. literalinclude:: examples/response/success-all.json
  :language: JSON

Status 400: Bad Request
-------------------------

Ocorre quando há algum problema nos parâmetros enviados.

.. literalinclude:: examples/response/fail.json
  :language: JSON



****************************
Consultar Categoria por ID
****************************

Referência
===========

:guilabel:`GET` ``https://api.ebranvo.com/v1/produtos/categorias/{id}``

Basta uma requisição GET simples para esse endpoint, informando o ID da categoria na URL para retornar a categoria em questão.

Resposta
=========

Status 200: OK
--------------------

.. literalinclude:: examples/response/success.json
  :language: JSON

Status 400: Bad Request
-------------------------

Ocorre quando há algum problema nos parâmetros enviados.

.. literalinclude:: examples/response/fail.json
  :language: JSON




***********************
Excluir Categoria
***********************

Referência
===========

:guilabel:`DELETE` ``https://api.ebranvo.com/v1/produtos/categorias/{id}``

Basta uma requisição DELETE simples para esse endpoint, informando o ID da categoria na URL para excluir a categoria em questão.

Resposta
=========

Status 200: OK
--------------------

.. literalinclude:: examples/response/success.json
  :language: JSON

Status 400: Bad Request
-------------------------

Ocorre quando há algum problema nos parâmetros enviados.

.. literalinclude:: examples/response/fail.json
  :language: JSON