###################
Foto
###################

A API e-Branvo permite o cadastro de fotos nos produtos tanto por link quanto por `uploaded file`.
No caso de link, as requisições podem ser feitas utilizando `application/json` e em caso de arquivo, deverá ser `multipart/form-data`,
caso contrário, o arquivo não será salvo corretamente.

A e-Branvo trata as fotos dos produtos em 3 tamanhos (P, M e G). Em caso de cadastro por link, os links dos 3 tamanhos deverão ser informados.
Caso contrário, o arquivo deverá ser enviado apenas em um tamanho (G), e a API irá realizar os redimensionamentos necessários para salvá-la nos 3 tamanhos.
O tamanho das fotos em pixels pode variar conforme a template da loja.

Para simular o envio de arquivos, confira a o tutorial "Simulando envio de arquivos no POSTMAN" da sessão de testes_ da documentação.

.. _testes: ./tests/index.html

********************
Adicionar Foto
********************

Referência
===========

:guilabel:`POST` ``https://api.ebranvo.com/v1/produtos/imagens``

.. include:: ./examples/table/image.rst

.. note::
   Parâmetros com asterisco ( * ) são obrigatórios


Exemplo de Requisição
======================

JSON
------

.. literalinclude:: ./examples/request/request.json
  :language: JSON

FORM-DATA
----------

.. include:: ./examples/request/request.rst

Resposta
=========

Status 201: CREATED
--------------------

.. literalinclude:: examples/response/success.json
  :language: JSON

Status 400: Bad Request
-------------------------

Ocorre quando há algum problema nos parâmetros enviados.

.. literalinclude:: examples/response/fail.json
  :language: JSON



*******************
Atualizar Foto
*******************

Referência
===========

:guilabel:`POST` ``https://api.ebranvo.com/v1/produtos/imagens``

A atualização de fotos, diferente dos outros itens, é feita por :guilabel:`POST`, pois o método :guilabel:`PUT` não aceita o envio de arquivos na requisição `multipart/form-data`.

.. include:: ./examples/table/image-upd.rst

.. note::
   Parâmetros com asterisco ( * ) são obrigatórios


Exemplo de Requisição
======================

JSON
------

.. literalinclude:: ./examples/request/request-upd.json
  :language: JSON


FORM-DATA
----------

.. include:: ./examples/request/request-upd.rst

Resposta
=========

Status 200: OK
--------------------

.. literalinclude:: examples/response/success-upd.json
  :language: JSON

Status 400: Bad Request
-------------------------

Ocorre quando há algum problema nos parâmetros enviados.

.. literalinclude:: examples/response/fail.json
  :language: JSON



**********************
Consultar Fotos
**********************

Referência
===========

:guilabel:`GET` ``https://api.ebranvo.com/v1/produtos/{id}/imagens``

Basta uma requisição GET simples para esse endpoint, informando o ID do produto na URL para retornar todas as fotos do produto em questão.

Resposta
=========

Status 200: OK
--------------------

.. literalinclude:: examples/response/success-all.json
  :language: JSON

Status 400: Bad Request
-------------------------

Ocorre quando há algum problema nos parâmetros enviados.

.. literalinclude:: examples/response/fail.json
  :language: JSON



****************************
Consultar Foto por ID
****************************

Referência
===========

:guilabel:`GET` ``https://api.ebranvo.com/v1/produtos/imagens/{id}``

Basta uma requisição GET simples para esse endpoint, informando o ID da foto na URL para retornar a foto em questão.

Resposta
=========

Status 200: OK
--------------------

.. literalinclude:: examples/response/success.json
  :language: JSON

Status 400: Bad Request
-------------------------

Ocorre quando há algum problema nos parâmetros enviados.

.. literalinclude:: examples/response/fail.json
  :language: JSON




***********************
Excluir Foto
***********************

Referência
===========

:guilabel:`DELETE` ``https://api.ebranvo.com/v1/produtos/imagens/{id}``

Basta uma requisição DELETE simples para esse endpoint, informando o ID da foto na URL para excluir a foto em questão.

Resposta
=========

Status 200: OK
--------------------

.. literalinclude:: examples/response/success.json
  :language: JSON

Status 400: Bad Request
-------------------------

Ocorre quando há algum problema nos parâmetros enviados.

.. literalinclude:: examples/response/fail.json
  :language: JSON