.. e-Branvo API documentation master file, created by
   sphinx-quickstart on Mon Nov  5 13:20:02 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

###################
Tipos de Variação
###################

Para trabalhar com variações nas lojas virtuais da e-Branvo, você precisa cadastrar os seus tipos e suas opções.
Por exemplo: Cores (tipo): Azul, Branco, Preto (opções), Tamanhos (tipo): P, M, G (opções).

****************************
Adicionar Tipos de Variação
****************************

Referência
===========

:guilabel:`POST` ``https://api.ebranvo.com/v1/produtos/tipoVariacao``

.. include:: ./examples/table/variationType.rst

.. note::
   Parâmetros com asterisco ( * ) são obrigatórios


Exemplo de Requisição
======================

.. literalinclude:: ./examples/request/request.json
  :language: JSON

Resposta
=========

Status 201: CREATED
--------------------

.. literalinclude:: examples/response/success.json
  :language: JSON

Status 400: Bad Request
-------------------------

Ocorre quando há algum problema nos parâmetros enviados.

.. literalinclude:: examples/response/fail.json
  :language: JSON



******************************
Atualizar Tipos de Variação
******************************

Referência
===========

:guilabel:`PUT` ``https://api.ebranvo.com/v1/produtos/tipoVariacao``

.. include:: ./examples/table/variationType-upd.rst

.. note::
   Parâmetros com asterisco ( * ) são obrigatórios


Exemplo de Requisição
======================

.. literalinclude:: ./examples/request/request-upd.json
  :language: JSON

Resposta
=========

Status 200: OK
--------------------

.. literalinclude:: examples/response/success.json
  :language: JSON

Status 400: Bad Request
-------------------------

Ocorre quando há algum problema nos parâmetros enviados.

.. literalinclude:: examples/response/fail.json
  :language: JSON



*****************************
Consultar Tipos de Variação
*****************************

Referência
===========

:guilabel:`GET` ``https://api.ebranvo.com/v1/produtos/tipoVariacao``

Basta uma requisição GET simples para esse endpoint para retornar todas os tipos de variação cadastrados em uma loja.

Resposta
=========

Status 200: OK
--------------------

.. literalinclude:: examples/response/success-all.json
  :language: JSON

Status 400: Bad Request
-------------------------

Ocorre quando há algum problema nos parâmetros enviados.

.. literalinclude:: examples/response/fail.json
  :language: JSON



***********************************
Consultar Tipos de Variação por ID
***********************************

Referência
===========

:guilabel:`GET` ``https://api.ebranvo.com/v1/produtos/tipoVariacao/{id}``

Basta uma requisição GET simples para esse endpoint, informando o ID do tipo de variação na URL para retornar o tipo de variação em questão.

Resposta
=========

Status 200: OK
--------------------

.. literalinclude:: examples/response/success.json
  :language: JSON

Status 400: Bad Request
-------------------------

Ocorre quando há algum problema nos parâmetros enviados.

.. literalinclude:: examples/response/fail.json
  :language: JSON




***************************
Excluir Tipos de Variação
***************************

Referência
===========

:guilabel:`DELETE` ``https://api.ebranvo.com/v1/produtos/tipoVariacao/{id}``

Basta uma requisição DELETE simples para esse endpoint, informando o ID do tipo de variação na URL para excluir o tipo de variação em questão.

Resposta
=========

Status 200: OK
--------------------

.. literalinclude:: examples/response/success.json
  :language: JSON

Status 400: Bad Request
-------------------------

Ocorre quando há algum problema nos parâmetros enviados.

.. literalinclude:: examples/response/fail.json
  :language: JSON