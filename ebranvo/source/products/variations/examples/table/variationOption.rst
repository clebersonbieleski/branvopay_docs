.. table::
   :widths: auto

   +--------------------------+----------------------+------------------------------------------------------+
   | Parâmetros               | Tipo                 | Descrição                                            |
   +==========================+======================+======================================================+
   | title*                   | string               | Nome/título da opção de variação                     |
   +--------------------------+----------------------+------------------------------------------------------+
   | active                   | boolean              | Opção de variação ativa?                             |
   +--------------------------+----------------------+------------------------------------------------------+
   | idType*                  | int                  | ID do type da opção de variação                      |
   +--------------------------+----------------------+------------------------------------------------------+