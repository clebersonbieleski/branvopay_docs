.. table::
   :widths: auto

   +--------------------------+-----------------+-------------------------------------------------------------------+
   | Parameters               | type            | Descrição                                                         |
   +==========================+=================+===================================================================+
   | id*                      | int             | ID do tipo de variação na e-Branvo                                |
   +--------------------------+-----------------+-------------------------------------------------------------------+
   | title                    | string          | Nome/título do tipo de variação                                   |
   +--------------------------+-----------------+-------------------------------------------------------------------+
   | active                   | int(0, 1)       | Tipo de variação ativo?                                           |
   +--------------------------+-----------------+-------------------------------------------------------------------+