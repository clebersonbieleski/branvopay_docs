#########
Variações
#########

===================
Variação do Produto
===================

------------------------------
Adicionar variação de produto
------------------------------

Referência
===========

:guilabel:`POST` ``https://api.ebranvo.com/v1/produtos/variacoes``

.. include:: ./examples/table/variationProduct.rst

.. note::
   Parâmetros com asterisco ( * ) são obrigatórios


Exemplo de Requisição
======================

.. literalinclude:: ./examples/request/variation/request.json
  :language: JSON

Resposta
=========

Status 201: CREATED
--------------------

É criada uma variação para cada `Opção de Variação`_.
Caso seja informado mais do que um `Tipo de Variação`_ (como no exemplo acima), será realizada a combinação das opções.

.. literalinclude:: examples/response/variation/success.json
  :language: JSON

Status 400: Bad Request
-------------------------

Ocorre quando há algum problema nos parâmetros enviados.

.. literalinclude:: examples/response/fail.json
  :language: JSON



------------------------------
Atualizar variação de produto
------------------------------

Referência
===========

:guilabel:`POST` ``https://api.ebranvo.com/v1/produtos/variacoes/atualizar``

.. note::
   A atualização de variações de produto, diferentemente de outros itens, deve ser feita usando o método :guilabel:`POST`, pois o método :guilabel:`PUT` não aceita `multipart/form-data` para o caso de envio de arquivos de imagens.

.. include:: ./examples/table/variationProduct-upd.rst

.. note::
   Parâmetros com asterisco ( * ) são obrigatórios

Explicando a funcionalidade das imagens:
    | Para atualização de variação de produto, são aceitos arrays de objetos e arrays de IDs de itens já existentes.
    | No caso de array de objetos, eles irão seguir as regras descritas na sessão do respectivo objeto.
    | No caso de array de IDs, os IDs serão validados e relacionados com a variação de produto em questão.
    | Caso informe no array IDs que ainda não estão relacionados a variação, eles serão validados e relacionados.
    | Caso informe um array sem IDs que já estão relacionados a variação, eles serão removidos.

Exemplo prático usando imagens:
    | Inseri uma variação com as imagens [1, 2, 3].
    | Fiz uma requisição de atualização, enviando no array `images` os valores [1, 2].
    | Nesse caso, a imagem 3 será removida.
    |
    | Inseri uma variação com as imagens [1, 2, 3].
    | Fiz uma requisição de atualização, enviando no array `images` os valores [1, 2, 3, 4].
    | Nesse caso, as imagens [1, 2, 3] serão mantidas, e a imagem 4 será adicionada a variação de produto.

Exemplo de Requisição
======================

.. literalinclude:: ./examples/request/variation/request-upd.json
  :language: JSON

Resposta
=========

Status 200: OK
--------------------

.. literalinclude:: examples/response/variation/success-upd.json
  :language: JSON

Status 400: Bad Request
-------------------------

Ocorre quando há algum problema nos parâmetros enviados.

.. literalinclude:: examples/response/fail.json
  :language: JSON



-------------------------------
Consultar variações de produto
-------------------------------


Referência
===========

:guilabel:`GET` ``https://api.ebranvo.com/v1/produtos/variacoes/pagina/{numero-pagina}``

A API realiza a consulta com paginação, onde a variável {numero-pagina} corresponde ao número da página desejado (1, 2, 3 ...) 
e são retornadas 50 variações de produtos por página.

Resposta
=========

Status 200: OK
--------------------

.. literalinclude:: examples/response/variation/success-all.json
  :language: JSON

Status 400: Bad Request
-------------------------

Ocorre quando há algum problema nos parâmetros enviados.

.. literalinclude:: examples/response/fail.json
  :language: JSON

------------------------------------
Consultar variação de produto por ID
------------------------------------

Referência
===========

:guilabel:`GET` ``https://api.ebranvo.com/v1/produtos/variacoes/{id}``

Basta uma requisição GET simples para esse endpoint, informando o ID da variação de produto na URL, para retornar a variação em questão.

Resposta
=========

Status 200: OK
--------------------

.. literalinclude:: examples/response/variation/success-one.json
  :language: JSON

Status 400: Bad Request
-------------------------

Ocorre quando há algum problema nos parâmetros enviados.

.. literalinclude:: examples/response/fail.json
  :language: JSON


----------------------------
Excluir variação de produto
----------------------------

Referência
===========

:guilabel:`DELETE` ``https://api.ebranvo.com/v1/produtos/variacoes/{id}``

Basta uma requisição DELETE simples para esse endpoint, informando o ID da variação na URL, para excluir a variação em questão.

Resposta
=========

Status 200: OK
--------------------

.. literalinclude:: examples/response/variation/success-one.json
  :language: JSON

Status 400: Bad Request
-------------------------

Ocorre quando há algum problema nos parâmetros enviados.

.. literalinclude:: examples/response/fail.json
  :language: JSON

------------

================
Tipo de Variação
================

--------------------------
Adicionar tipo de variação
--------------------------

Referência
===========

:guilabel:`POST` ``https://api.ebranvo.com/v1/produtos/tipoVariacao``

.. include:: ./examples/table/variationType.rst

.. note::
   Parâmetros com asterisco ( * ) são obrigatórios


Exemplo de Requisição
======================

.. literalinclude:: ./examples/request/type/request.json
  :language: JSON

Resposta
=========

Status 201: CREATED
--------------------

.. literalinclude:: examples/response/type/success.json
  :language: JSON

Status 400: Bad Request
-------------------------

Ocorre quando há algum problema nos parâmetros enviados.

.. literalinclude:: examples/response/fail.json
  :language: JSON



---------------------------
Atualizar tipo de variação
---------------------------

Referência
===========

:guilabel:`PUT` ``https://api.ebranvo.com/v1/produtos/tipoVariacao``

.. include:: ./examples/table/variationType-upd.rst

.. note::
   Parâmetros com asterisco ( * ) são obrigatórios

Explicando a funcionalidade das options:
    | Para atualização de tipos de variação, são aceitos arrays de objetos do tipo option e arrays de strings contendo o title de options (a serem criadas ou já existentes).
    | No caso de array de objetos option, eles irão seguir as regras descritas em sua respectiva sessão.
    | No caso de array de strings, elas serão validadas e relacionadas com o tipo de variação em questão.
    | Caso informe no array strings que ainda não sejam opções, elas serão validadas, criadas e relacionadas com o tipo de variação.
    | Caso informe um array sem strings que correspondam a opções que já estão relacionadas ao tipo de variação, elas serão removidas.

Exemplo:
    | Inseri um tipo de variação com as opções ["P", "M", "G", "GG"].
    | Fiz uma requisição de atualização, enviando no array `options` os valores ["P", "M", "G"].
    | Nesse caso, a opção GG será removida.
    |
    | Inseri um tipo de variação com as opções ["P", "M", "G", "GG"].
    | Fiz uma requisição de atualização, enviando no array `options` os valores ["PP", "P", "M", "G", "GG"].
    | Nesse caso, as opções ["P", "M", "G", "GG"] serão mantidas, e a opção "PP" será criada e adicionada ao tipo de variação.

Exemplo de Requisição
======================

.. literalinclude:: ./examples/request/type/request-upd.json
  :language: JSON

Resposta
=========

Status 200: OK
--------------------

.. literalinclude:: examples/response/type/success-upd.json
  :language: JSON

Status 400: Bad Request
-------------------------

Ocorre quando há algum problema nos parâmetros enviados.

.. literalinclude:: examples/response/fail.json
  :language: JSON



---------------------------
Consultar tipos de variação
---------------------------


Referência
===========

:guilabel:`GET` ``https://api.ebranvo.com/v1/produtos/tipoVariacao``

Basta uma requisição GET simples para esse endpoint para retornar todos os tipos de variações cadastrados em uma loja.

Resposta
=========

Status 200: OK
--------------------

.. literalinclude:: examples/response/type/success-all.json
  :language: JSON

Status 400: Bad Request
-------------------------

Ocorre quando há algum problema nos parâmetros enviados.

.. literalinclude:: examples/response/fail.json
  :language: JSON



---------------------------------
Consultar tipo de variação por ID
---------------------------------

Referência
===========

:guilabel:`GET` ``https://api.ebranvo.com/v1/produtos/tipoVariacao/{id}``

Basta uma requisição GET simples para esse endpoint, informando o ID do tipo da variação na URL, para retornar o tipo em questão.

Resposta
=========

Status 200: OK
--------------------

.. literalinclude:: examples/response/type/success.json
  :language: JSON

Status 400: Bad Request
-------------------------

Ocorre quando há algum problema nos parâmetros enviados.

.. literalinclude:: examples/response/fail.json
  :language: JSON




------------------------
Excluir tipo de variação
------------------------

Referência
===========

:guilabel:`DELETE` ``https://api.ebranvo.com/v1/produtos/tipoVariacao/{id}``

Basta uma requisição DELETE simples para esse endpoint, informando o ID do tipo da variação na URL, para excluir o tipo em questão.

Resposta
=========

Status 200: OK
--------------------

.. literalinclude:: examples/response/type/success.json
  :language: JSON

Status 400: Bad Request
-------------------------

Ocorre quando há algum problema nos parâmetros enviados.

.. literalinclude:: examples/response/fail.json
  :language: JSON

------------

==================
Opção de Variação
==================

---------------------------
Adicionar opção de variação
---------------------------

Referência
===========

:guilabel:`POST` ``https://api.ebranvo.com/v1/produtos/opcaoVariacao``

.. include:: ./examples/table/variationOption.rst

.. note::
   Parâmetros com asterisco ( * ) são obrigatórios


Exemplo de Requisição
======================

.. literalinclude:: ./examples/request/option/request.json
  :language: JSON

Resposta
=========

Status 201: CREATED
--------------------

.. literalinclude:: examples/response/option/success.json
  :language: JSON

Status 400: Bad Request
-------------------------

Ocorre quando há algum problema nos parâmetros enviados.

.. literalinclude:: examples/response/fail.json
  :language: JSON



---------------------------
Atualizar opção de variação
---------------------------

Referência
===========

:guilabel:`PUT` ``https://api.ebranvo.com/v1/produtos/opcaoVariacao``

.. include:: ./examples/table/variationOption-upd.rst

.. note::
   Parâmetros com asterisco ( * ) são obrigatórios


Exemplo de Requisição
======================

.. literalinclude:: ./examples/request/option/request-upd.json
  :language: JSON

Resposta
=========

Status 200: OK
--------------------

.. literalinclude:: examples/response/option/success-upd.json
  :language: JSON

Status 400: Bad Request
-------------------------

Ocorre quando há algum problema nos parâmetros enviados.

.. literalinclude:: examples/response/fail.json
  :language: JSON



-----------------------------
Consultar opções de variação
-----------------------------


Referência
===========

:guilabel:`GET` ``https://api.ebranvo.com/v1/produtos/opcaoVariacao``

Basta uma requisição GET simples para esse endpoint para retornar todas as opções de variações cadastradas em uma loja.

Resposta
=========

Status 200: OK
--------------------

.. literalinclude:: examples/response/option/success-all.json
  :language: JSON

Status 400: Bad Request
-------------------------

Ocorre quando há algum problema nos parâmetros enviados.

.. literalinclude:: examples/response/fail.json
  :language: JSON



----------------------------------
Consultar opção de variação por ID
----------------------------------

Referência
===========

:guilabel:`GET` ``https://api.ebranvo.com/v1/produtos/opcaoVariacao/{id}``

Basta uma requisição GET simples para esse endpoint, informando o ID da opção da variação na URL, para retornar a opção em questão.

Resposta
=========

Status 200: OK
--------------------

.. literalinclude:: examples/response/option/success.json
  :language: JSON

Status 400: Bad Request
-------------------------

Ocorre quando há algum problema nos parâmetros enviados.

.. literalinclude:: examples/response/fail.json
  :language: JSON


-------------------------
Excluir opção de variação
-------------------------

Referência
===========

:guilabel:`DELETE` ``https://api.ebranvo.com/v1/produtos/opcaoVariacao/{id}``

Basta uma requisição DELETE simples para esse endpoint, informando o ID da opção da variação na URL, para excluir a opção em questão.

Resposta
=========

Status 200: OK
--------------------

.. literalinclude:: examples/response/option/success.json
  :language: JSON

Status 400: Bad Request
-------------------------

Ocorre quando há algum problema nos parâmetros enviados.

.. literalinclude:: examples/response/fail.json
  :language: JSON